package com.hamatim.library;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.hamatim.hmt_ads_interstitial.HMTAdsInterstitial;
import com.hamatim.hmt_splash.ActivityBaseSplash;

public class ActivitySplash extends ActivityBaseSplash {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected Class<? extends AppCompatActivity> getTargetActivity() {
        return MainActivity.class;
    }

    @Override
    protected void navToTargetActivity() {
        HMTAdsInterstitial.get().loadNewAdsAndShow(this, () -> super.navToTargetActivity());
    }

    @Override
    protected long getSleepTimeInMillis() {
        return 100;
    }

}
