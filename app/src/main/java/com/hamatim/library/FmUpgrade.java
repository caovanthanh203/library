package com.hamatim.library;

import com.hamatim.fragment.FmBaseUpgrade;
import com.hamatim.helper.VoidCallBack;
import com.hamatim.locale.HMTLocale;

public class FmUpgrade extends FmBaseUpgrade {

    @Override
    protected String getUpgradePackageName() {
        return "com.hamatim.example";
    }

    @Override
    protected String getUpgradeValues() {
        return super.getUpgradeValues(getString(R.string.ads), "Reduce size by 50%");
    }

    @Override
    protected boolean hasUpgradeIcon() {
        return false;
    }

    @Override
    protected void onUpgradeLater() {
        HMTLocale.showDialog(getContext(), new VoidCallBack() {
            @Override
            public void callback() {
                getActivity().recreate();
            }
        });
    }
}
