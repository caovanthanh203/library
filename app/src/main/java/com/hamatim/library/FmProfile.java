package com.hamatim.library;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hamatim.fragment.FmBase;
import com.hamatim.fragment.FmPlaceholder;
import com.hamatim.picasso.PicassoBase;

public class FmProfile extends FmBase {

    private ImageView imvTest;

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_home;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        imvTest = root.findViewById(R.id.imvTest);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        PicassoBase.getTLSPicasso()
                .load("https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg")
                .into(imvTest);
    }
}
