package com.hamatim.library;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.hamatim.fragment.VHBaseWrapper;

public class VHParent extends VHBaseWrapper {
    public TextView tvName;

    public VHParent(@NonNull View itemView) {
        super(itemView);
        tvName = getView(R.id.tvName);
    }
}
