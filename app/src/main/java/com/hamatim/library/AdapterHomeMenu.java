package com.hamatim.library;

import android.content.Context;
import android.view.View;

import com.hamatim.fragment_menu.AdapterMenu;
import com.hamatim.fragment_menu.VHMenu;

public class AdapterHomeMenu extends AdapterMenu {
    public AdapterHomeMenu(Context mContext) {
        super(mContext);
    }

    @Override
    protected VHMenu onCreateViewHolder(View root, int viewType) {
        return super.onCreateViewHolder(root, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getViewType();
    }

    @Override
    protected int getLayoutResId(int viewType) {
        if (viewType == 1){
            return R.layout.layout_item_header_text;
        }
        return R.layout.layout_item_menu_text;
    }
}
