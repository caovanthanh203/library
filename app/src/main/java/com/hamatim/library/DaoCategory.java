package com.hamatim.library;

import androidx.room.Dao;

import com.hamatim.database.DAOBase;

@Dao
public abstract class DaoCategory extends DAOBase<Category> {

    @Override
    protected String getTableName() {
        return "category";
    }

}
