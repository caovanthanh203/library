package com.hamatim.library;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.hamatim.fragment.FmBase;
import com.hamatim.fragment.FmBaseListWrapper;
import com.hamatim.helper.HelperLayout;
import com.hamatim.picasso.PicassoBase;

import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;

public class FmWrapper extends FmBaseListWrapper<AdapterParent> {

    private ImageView imvTest;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        imvTest = root.findViewById(R.id.imvTest);
        return root;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return HelperLayout.getLayoutManager(getContext());
    }

    @Override
    protected AdapterParent onCreateAdapter() {
        AdapterParent adapterParent = new AdapterParent(getContext());
        AdapterParent adapterParentOfParent = new AdapterParent(getContext());
        AdapterParent adapterChild = new AdapterParent(getContext());
//        AdapterChild adapterChild = new AdapterChild(getContext());

        List<String> parentOfParent = Arrays.asList("Parent of Parent 1", "Parent of Parent 2", "Parent of Parent 3");
        List<String> parent = Arrays.asList("Parent 1", "Parent 2", "Parent 3");
        List<String> child = Arrays.asList("Child 1", "Child 2", "Child 3");

        adapterParentOfParent.setmList(parentOfParent);
        adapterParent.setmList(parent);
        adapterChild.setmList(child);

//        adapterParent.setAdapterBaseWrapper(adapterChild);
        adapterParent.setAdapterBaseWrapper(adapterChild);
        adapterParentOfParent.setAdapterBaseWrapper(adapterParent);
        return adapterParentOfParent;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
//        PicassoBase.getTLSPicasso()
//                .load("https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg")
//                .into(imvTest);
    }
}
