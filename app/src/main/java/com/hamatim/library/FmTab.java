package com.hamatim.library;

import com.google.android.material.tabs.TabLayout;
import com.hamatim.fragment.FmBaseTab;

public class FmTab extends FmBaseTab {
    @Override
    protected int getNavigationGraphResId() {
        return R.navigation.tab_graph;
    }

    @Override
    public int getHostFragmentResId() {
        return R.id.nav_tab_host_fragment;
    }

    @Override
    protected TabLayout.OnTabSelectedListener getTabChangeListener() {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0:
                        childNavUp(R.id.fmTabOne);
                        break;
                    case 1:
                        childNavUp(R.id.fmTabTwo);
                        break;
                    case 2:
                        childNavUp(R.id.fmTabThree);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

}
