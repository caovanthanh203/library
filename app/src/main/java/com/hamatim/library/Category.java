package com.hamatim.library;

import androidx.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hamatim.database.EntityBase;

@Entity
public class Category extends EntityBase {

    @SerializedName("name")
    @Expose
    protected String name;

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
