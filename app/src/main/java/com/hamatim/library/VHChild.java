package com.hamatim.library;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.hamatim.fragment.VHBaseWrapper;

public class VHChild extends VHBaseWrapper {
    public TextView tvName;

    public VHChild(@NonNull View itemView) {
        super(itemView);
        tvName = getView(R.id.tvName);
    }
}
