package com.hamatim.library;

import androidx.room.Dao;

import com.hamatim.database.DAOBase;

@Dao
public abstract class DaoPerson extends DAOBase<Person> {

    @Override
    protected String getTableName() {
        return "person";
    }

}
