package com.hamatim.library;

import android.content.Context;
import androidx.room.Database;
import com.hamatim.database.DBBase;

import static androidx.room.Room.databaseBuilder;

@Database(entities = {Category.class, Person.class}, version = 2)
public abstract class DBMain extends DBBase {

    //@Database(entities = {Category.class}, version = 1)
    private static DBMain instance;

    public static void init(Context context, String dbName) {
        instance = databaseBuilder(context, DBMain.class, dbName)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    private static DBMain get() {
        if (instance == null) {
            throw new RuntimeException("You have to call DBMain.init(context, dbName) first!!!");
        }
        return instance;
    }

    //roomDao
    abstract DaoCategory createDaoCategory();

    public static DaoCategory getDaoCategory() {
        return get().createDaoCategory();
    }

    abstract DaoPerson createDaoPerson();

    public static DaoPerson getDaoPerson() {
        return get().createDaoPerson();
    }

}
