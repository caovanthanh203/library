package com.hamatim.library;

import android.content.Context;
import android.view.View;

import com.hamatim.fragment.AdapterBaseWrapper;

public class AdapterChild extends AdapterBaseWrapper<String, VHChild> {
    public AdapterChild(Context mContext) {
        super(mContext);
    }

    @Override
    protected VHChild onCreateViewHolder(View root) {
        return new VHChild(root);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.simple_item;
    }

    @Override
    protected void bindSelfViewHolder(VHChild holder, int position) {
        holder.tvName.setText("Child " + position);
    }

    @Override
    public int getItemViewType(int position) {
        return hashCode();
    }
}
