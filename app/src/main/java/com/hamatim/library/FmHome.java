 package com.hamatim.library;

import android.content.Context;

import com.hamatim.fragment_menu.AdapterMenu;
import com.hamatim.fragment_menu.FmBaseMenu;
import com.hamatim.fragment_menu.Menu;

import java.util.List;

public class FmHome extends FmBaseMenu {
    @Override
    protected AdapterMenu onCreateAdapter(Context context) {
        return new AdapterHomeMenu(context);
    }

    @Override
    protected List<Menu> getMenuList() {
        List<Menu> menuList = super.getMenuList();
        menuList.clear();
        menuList.add(new Menu("Hello", this::navToProfile, 2, 1));
        menuList.add(new Menu("A", () -> navTo(R.id.action_fmHome_to_fmTab)));
        menuList.add(new Menu("Upgrade", () -> navTo(R.id.action_fmHome_to_fmUpgrade)));
        menuList.add(new Menu("Upgrade", () -> navTo(R.id.action_fmHome_to_fmUpgrade), 3));
        menuList.add(new Menu("12123", () -> navTo(R.id.action_fmHome_to_fmUpgrade),3));
        menuList.add(new Menu("Upgrade", () -> navTo(R.id.action_fmHome_to_fmUpgrade), 4, 1));
        menuList.add(new Menu("XXXX", () -> navTo(R.id.action_fmHome_to_fmUpgrade)));
        return menuList;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_list_fab;
    }

    private void navToProfile() {
//        HMTLocale.showDialog(getContext(), new VoidCallBack() {
//            @Override
//            public void callback() {
//                getActivity().recreate();
//            }
//        });
//        MainActivity.showInterstitialAds(getActivity(), () -> {
//            super.onResume();
//            navTo(R.id.action_fmHome_to_fmProfile);
//        });
    }
}