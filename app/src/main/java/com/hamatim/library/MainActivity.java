package com.hamatim.library;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.hamatim.helper.HelperPref;
import com.hamatim.locale.HMTLocale;
import com.hamatim.navigation_ads_banner.ActivityBaseGraphBanner;
//import com.hamatim.navigation_ads_interstitial.ActivityBaseInterstitial;
import com.hamatim.helper.HMTAdsLimit;
import com.hamatim.picasso.PicassoBase;

public class MainActivity extends ActivityBaseGraphBanner {
    private static final String TAG = "HMTDriveMainActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        PicassoBase.init(getApplication());
        HelperPref.init(getApplication(), getApplicationId());
        HMTLocale.init(getApplication(), getApplicationId());
        HMTLocale.applyLocale(this);
        HMTAdsLimit.init()
        .enableDebug(true)
        .secondLimit(6000)
        .clickLimit(2)
        .fraudCallback(strikeCount -> Log.d(TAG, "Strike count " + strikeCount));

        DBMain.init(this, "main");
        DBMain.getDaoCategory().insert(new Category("category"));
        DBMain.getDaoPerson().insert(new Person("person"));

        for (Category category: DBMain.getDaoCategory().readAll()){
            Log.d(TAG, "db category: " + category.getName());
        }

        for (Person person: DBMain.getDaoPerson().readAll()){
            Log.d(TAG, "db person: " + person.getFirstName());
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    protected boolean shouldCheckFraud() {
        return true;
    }

    @Override
    protected String getApplicationId() {
        return BuildConfig.APPLICATION_ID;
    }

    @Override
    protected int getHomeDestination() {
        return R.id.fmHome;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_activity_base_graph_mid_banner;
    }

    @Override
    protected int getNavigationGraphResId() {
        return R.navigation.mobile_graph;
    }

    @Override
    public int getHostFragmentResId() {
        return R.id.nav_host_fragment;
    }

    @Override
    protected boolean shouldShowRatingDialog() {
        return true;
    }

    @Override
    protected boolean debugRatingDialog() {
        return true;
    }

    @Override
    protected boolean forceNotLoadAds() {
        return true;
    }
}