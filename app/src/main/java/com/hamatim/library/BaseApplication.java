package com.hamatim.library;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.hamatim.hmt_ads_interstitial.HMTAdsInterstitial;
import com.hamatim.hmt_base.ApplicationBaseMultiDex;

public class BaseApplication extends ApplicationBaseMultiDex {

    @Override
    public void onCreate() {
        super.onCreate();
        MobileAds.initialize(this, initializationStatus -> {});
        HMTAdsInterstitial.init(this);
//        HMTAdsInterstitial.get().loadNewAds();
    }
}
