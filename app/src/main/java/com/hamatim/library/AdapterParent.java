package com.hamatim.library;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.hamatim.fragment.AdapterBaseWrapper;
import com.hamatim.fragment.VHBaseWrapper;

public class AdapterParent extends AdapterBaseWrapper<String, VHParent> {
    public AdapterParent(Context mContext) {
        super(mContext);
    }

    @Override
    protected VHParent onCreateViewHolder(View root) {
        return new VHParent(root);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.simple_item;
    }

    @Override
    protected void bindSelfViewHolder(VHParent holder, int position) {
        holder.tvName.setText(getmList().get(position));
    }

}
