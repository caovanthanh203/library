package com.hamatim.picasso;

import android.app.Application;
import android.util.Log;
import android.widget.ImageView;

import com.hamatim.helper.HelperSingleton;
import com.hamatim.network_client.ClientBase;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class PicassoBase {

    private static final String TAG = "PicassoBase";
    private Picasso picasso, picassoTLS;
    private Picasso.Builder builder;
    private boolean loggingEnabled = false;

    public static PicassoBase init(Application application){
        getInstance().setBuilder(new Picasso.Builder(application));
        return getInstance();
    }

    private void setBuilder(Picasso.Builder builder) {
        this.builder = builder;
    }

    private static PicassoBase getInstance() {
        return HelperSingleton.get(PicassoBase.class);
    }

    public static Picasso getPicasso(){
        return getInstance().getPicassoInstance();
    }

    public static Picasso getTLSPicasso(){
        return getInstance().getPicassoTLSInstance();
    }

    private Picasso getPicassoInstance() {
        if (picasso == null) {
            try {
                picasso = builder
                        .loggingEnabled(loggingEnabled)
                        .downloader(new OkHttp3Downloader(ClientBase.getHttpsClient(loggingEnabled, false)))
                        .build();
            } catch (Exception exception){
                Log.e(TAG, "getPicassoInstance: ", exception);
                picasso = Picasso.get();
            }
        }
        return picasso;
    }

    private Picasso getPicassoTLSInstance() {
        if (picassoTLS == null) {
            try {
                picassoTLS = builder
                        .loggingEnabled(loggingEnabled)
                        .downloader(new OkHttp3Downloader(ClientBase.getHttpsClient(loggingEnabled, true)))
                        .build();
            } catch (Exception exception){
                Log.e(TAG, "getPicassoInstance: call PicassoBase.init(application) first!");
                Log.e(TAG, "getPicassoInstance: ", exception);
                picassoTLS = Picasso.get();
            }
        }
        return picassoTLS;
    }

    public void loggingEnabled(boolean loggingEnabled){
        this.loggingEnabled = loggingEnabled;
    };

}
