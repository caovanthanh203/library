package com.hamatim.google_drive.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;
import com.hamatim.google_drive.R;
import com.hamatim.google_drive.app.AppActivity;
import com.hamatim.google_drive.data.DBConstants;
import com.hamatim.google_drive.data.InfoRepository;
import com.hamatim.google_drive.util.HMTGoogleAuth;
import com.hamatim.google_drive.util.HMTGoogleDrive;

import java.io.File;
import java.io.IOException;

public class HMTDriveMainActivity extends AppActivity {

    private static final String TAG = "HMTDriveMainActivity";

    private static final String GOOGLE_DRIVE_DB_LOCATION = "db";
    private static final int GOOGLE_SIGN_IN_REQUEST = 1010;

    private Button googleSignIn;
    private Group contentViews;
    private EditText inputToDb;
    private Button writeToDb;
    private Button saveToGoogleDrive;
    private Button restoreFromDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hmt_google_drive_activity_main);
        findViews();
        initViews();
    }

    protected void onGoogleDriveSignedInSuccess(Drive driveApi) {
        showMessage(R.string.message_drive_client_is_ready);
        googleSignIn.setVisibility(View.GONE);
        contentViews.setVisibility(View.VISIBLE);
    }

    protected void onGoogleSignedInFailed(ApiException exception) {
        showMessage(R.string.message_google_sign_in_failed);
        Log.e(TAG, "error google drive sign in", exception);
    }

    private void findViews() {
        googleSignIn = findViewById(R.id.google_sign_in);
        contentViews = findViewById(R.id.content_views);
        inputToDb = findViewById(R.id.edit_text_db_input);
        writeToDb = findViewById(R.id.write_to_db);
        saveToGoogleDrive = findViewById(R.id.save_to_google_drive);
        restoreFromDb = findViewById(R.id.restore_from_db);
    }

    private void initViews() {
        googleSignIn.setOnClickListener(v -> performSignIn());

        writeToDb.setOnClickListener(v -> {
            String text = inputToDb.getText().toString();
            InfoRepository repository = new InfoRepository(getApplicationContext());
            repository.writeInfo(text);
        });

        saveToGoogleDrive.setOnClickListener(v -> {
            File db = new File(DBConstants.DB_LOCATION);

            HMTGoogleDrive.getRepository().uploadFile(db, GOOGLE_DRIVE_DB_LOCATION)
                    .addOnSuccessListener(r -> showMessage("Upload success"))
                    .addOnFailureListener(e -> {
                        Log.e(TAG, "error upload file", e);
                        showMessage("Error upload");
                    });
        });

        restoreFromDb.setOnClickListener(v -> {
            File db = new File(DBConstants.DB_LOCATION);
            db.getParentFile().mkdirs();
            db.delete();
            HMTGoogleDrive.getRepository().queryFiles()
                    .addOnCompleteListener(new OnCompleteListener<FileList>() {
                        @Override
                        public void onComplete(@NonNull Task<FileList> task) {
                            final FileList fileList = task.getResult();
                            if (fileList == null) {
                                return;
                            }
                            for (com.google.api.services.drive.model.File f : fileList.getFiles()) {
                                Log.d(TAG, "getName: " + f.getName());
                                Log.d(TAG, "version: " + f.getVersion());
                                Log.d(TAG, "getModifiedTime: " + f.getModifiedTime());
                                Log.d(TAG, "getCreatedTime: " + f.getCreatedTime());
                            }
                        }
                    });
            HMTGoogleDrive.getRepository().downloadFile(db, GOOGLE_DRIVE_DB_LOCATION)
                    .addOnSuccessListener(r -> {
                        InfoRepository repository = new InfoRepository(getApplicationContext());
                        String infoText = repository.getInfo();
                        inputToDb.setText(infoText);
                        showMessage("Retrieved");
                    })
                    .addOnFailureListener(e -> {
                        Log.e(TAG, "error download file", e);
                        showMessage("Error download");
                    });
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN_REQUEST) {
            HMTGoogleAuth.handleSigninResult(
                    data,
                    this::onGoogleSignedInSuccess,
                    this::onGoogleSignedInFailed
            );
        }
    }

    private void onGoogleSignedInSuccess(GoogleSignInAccount googleSignInAccount) {
        HMTGoogleDrive.init(this, googleSignInAccount, this::onGoogleDriveSignedInSuccess);
    }

    private void performSignIn() {
        Intent intent = HMTGoogleAuth.getSigninIntent(this, HMTGoogleAuth.getGoogleDriveSignInOptions());
        startActivityForResult(intent, GOOGLE_SIGN_IN_REQUEST);
    }
}
