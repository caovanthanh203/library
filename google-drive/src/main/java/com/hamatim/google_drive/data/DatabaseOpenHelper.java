package com.hamatim.google_drive.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.hamatim.google_drive.app.App;

import static com.hamatim.google_drive.data.DBConstants.*;
import static com.hamatim.google_drive.data.DBConstants.DATABASE_CREATE;
import static com.hamatim.google_drive.data.DBConstants.DB_NAME;
import static com.hamatim.google_drive.data.DBConstants.DB_VERSION;

class DatabaseOpenHelper extends SQLiteOpenHelper {

    static SQLiteDatabase getAppDatabase(Context context) {
        final DatabaseOpenHelper helper = new DatabaseOpenHelper(context);
        return helper.getWritableDatabase();
    }

    private DatabaseOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        onCreate(database);
    }
}
