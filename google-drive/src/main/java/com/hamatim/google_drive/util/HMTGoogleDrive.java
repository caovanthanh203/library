package com.hamatim.google_drive.util;

import android.content.Context;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.hamatim.callback.HMTDTOCallBack;
import com.hamatim.google_drive.google.GoogleDriveApiDataRepository;

import java.util.ArrayList;
import java.util.List;

public class HMTGoogleDrive {

    private static GoogleDriveApiDataRepository repository;

    public static void init(
            Context context,
            GoogleSignInAccount signInAccount,
            HMTDTOCallBack<Drive> success
    ){
        List<String> scopes = new ArrayList<>();
        scopes.add(DriveScopes.DRIVE_APPDATA);

        GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(context, scopes);
        credential.setSelectedAccount(signInAccount.getAccount());

        Drive.Builder builder = new Drive.Builder(
                AndroidHttp.newCompatibleTransport(),
                new GsonFactory(),
                credential
        );
        String appName = "HMT Developer";
        Drive driveApi = builder
                .setApplicationName(appName)
                .build();

        repository = new GoogleDriveApiDataRepository(driveApi);

        success.callback(driveApi);
    }

    public static GoogleDriveApiDataRepository getRepository(){
        if (repository == null){
            throw new RuntimeException("Please HMTGoogleDrive.init() first!");
        }
        return repository;
    }

    public static void clearSession(){
        repository = null;
    }

    public static boolean isGrant(GoogleSignInAccount googleSignInAccount){
        return googleSignInAccount.getGrantedScopes().contains(DriveScopes.DRIVE_APPDATA);
    }

}
