package com.hamatim.google_drive.util;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.hamatim.callback.HMTDTOCallBack;
import com.hamatim.callback.HMTVoidCallBack;

public class HMTGoogleAuth {

    public static Intent getSigninIntent(Context context, GoogleSignInOptions signInOptions){
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(context, signInOptions);
        return googleSignInClient.getSignInIntent();
    }

    public static GoogleSignInOptions getGoogleDriveSignInOptions() {
        Scope scopeDriveAppFolder = new Scope(Scopes.DRIVE_APPFOLDER);
        return new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(scopeDriveAppFolder)
                .build();
    }

    public static void logout(Context context, GoogleSignInOptions signInOptions, HMTVoidCallBack onComplete){
        GoogleSignIn.getClient(context, signInOptions)
                .signOut()
                .addOnCompleteListener(task -> {
                    try {
                        onComplete.callback();
                    } catch (Exception exception){
                        exception.printStackTrace();
                    }
                });
    }

    public static void handleSigninResult(Intent intent,
                                          HMTDTOCallBack<GoogleSignInAccount> success,
                                          HMTDTOCallBack<ApiException> fail
    ){
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            success.callback(account);
        } catch (ApiException e) {
            fail.callback(e);
        }
    }

    public static void checkSignInStatus(Context context,
                                         HMTDTOCallBack<GoogleSignInAccount> success,
                                         HMTVoidCallBack fail){
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(context);
        try {
            if (googleSignInAccount != null){
                success.callback(googleSignInAccount);
            } else {
                fail.callback();
            }
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static boolean checkSignInStatus(Context context){
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(context);
        return googleSignInAccount != null;
    }

}
