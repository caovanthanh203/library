package com.hamatim.google_drive.util;

import android.content.Context;

import com.google.android.gms.common.api.ApiException;
import com.hamatim.callback.HMTDTOCallBack;
import com.hamatim.callback.HMTVoidCallBack;
import com.hamatim.google_drive.util.HMTGoogleDrive;

import java.io.File;

public class BackupManager {
    private static final String TAG = "BackupManager";

    public static void getLastFile(String filename,
                                   HMTDTOCallBack<com.google.api.services.drive.model.File> successCalback,
                                   HMTDTOCallBack<Exception> failCallback
    ){
        try {
            HMTGoogleDrive.getRepository()
                    .getLastFile(filename)
                    .addOnSuccessListener(file -> {
                        try {
                            successCalback.callback(file);
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    })
                    .addOnFailureListener(e -> {
                        try {
                            failCallback.callback(e);
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    });
        } catch (Exception exception){
            exception.printStackTrace();
//            failCallback.callback();
        }
    }

    public static void performBackup(Context context, String filename, HMTVoidCallBack successCalback, HMTVoidCallBack failCallback){
        File database = context.getDatabasePath(filename);
        try {
            HMTGoogleDrive.getRepository()
                    .uploadFile(database, filename)
                    .addOnSuccessListener(r -> {
                        try {
                            successCalback.callback();
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    })
                    .addOnFailureListener(e -> {
                        try {
                            failCallback.callback();
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    });
        } catch (Exception exception){
            exception.printStackTrace();
            failCallback.callback();
        }
    }

    public static void performRestore(Context context, String filename, HMTVoidCallBack successCalback, HMTDTOCallBack<Exception> failCallback){
        File database = context.getDatabasePath(filename);
        try {
            HMTGoogleDrive.getRepository().downloadFile(database, filename)
                .addOnSuccessListener(e -> {
                    try {
                        successCalback.callback();
                    } catch (Exception exception){
                        exception.printStackTrace();
                    }
                })
                .addOnFailureListener(e -> {
                    try {
                        failCallback.callback(e);
                    } catch (Exception exception){
                        exception.printStackTrace();
                    }
                });
        } catch (Exception exception){
            exception.printStackTrace();
            failCallback.callback(exception);
        }
    }

    public static void performRestore(Context context, String remoteFile, String targetFile, HMTVoidCallBack successCalback, HMTDTOCallBack<Exception> failCallback){
        File file = context.getDatabasePath(targetFile);
        try {
            HMTGoogleDrive.getRepository().downloadFile(file, remoteFile)
                    .addOnSuccessListener(e -> {
                        try {
                            successCalback.callback();
                        } catch (Exception exception){
                            exception.printStackTrace();
                        }
                    })
                    .addOnFailureListener(e -> {
                        try {
                            failCallback.callback(e);
                        } catch (Exception exception){
                            exception.printStackTrace();
                        }
                    });
        } catch (Exception exception){
            exception.printStackTrace();
            failCallback.callback(exception);
        }
    }

}
