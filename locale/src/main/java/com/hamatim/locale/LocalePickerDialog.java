package com.hamatim.locale;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class LocalePickerDialog extends DialogFragment {

//    private OnLocaleDialogConfirmClick onLocaleDialogConfirmClick;
    private int checkedLocaleId = 0, currentLocaleId = 0;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            checkedLocaleId = savedInstanceState.getInt("checkedLocaleId");
            currentLocaleId = savedInstanceState.getInt("currentLocaleId");
        }
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getText(R.string.timer_dialog_change_language_title))
                .setSingleChoiceItems(R.array.locale_entries, currentLocaleId, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        checkedLocaleId = i;
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (checkedLocaleId != currentLocaleId) {
                            dismiss();
//                            onLocaleDialogConfirmClick.OnLocaleChange(checkedLocaleId);
                        } else {
                            dismiss();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dismiss();
                    }
                });
        return builder.create();
    }

    public void setCurrentLocaleId(int localeId){
        this.currentLocaleId = localeId;
    }

//    public void setOnLocaleDialogConfirmClick(OnLocaleDialogConfirmClick onLocaleDialogConfirmClick) {
//        this.onLocaleDialogConfirmClick = onLocaleDialogConfirmClick;
//    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("checkedLocaleId", checkedLocaleId);
        outState.putInt("currentLocaleId", currentLocaleId);
    }

}
