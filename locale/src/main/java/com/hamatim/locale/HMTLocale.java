package com.hamatim.locale;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.hamatim.helper.HelperPref;
import com.hamatim.helper.VoidCallBack;

import java.util.ArrayList;
import java.util.Locale;

public class HMTLocale {
    private static final String TAG = "HMTLocale";
    private static final String THEME_LOCALE_CODE_PREFERENCE_KEY = "THEME_LOCALE_CODE_PREFERENCE_KEY";
    private static final String THEME_LOCALE_CODE_DEFAULT_VALUE = "en";
    private static String[] localeCodes;

    public static void init(Application application, String applicationId){
//        Log.d(TAG, "init");
        HelperPref.init(application, applicationId);
        localeCodes = getLocaleCodes(application);
    }

    public static void applyLocale(Context context){
        String localeCode = HelperPref.get().getStringValueWithDefault(
                THEME_LOCALE_CODE_PREFERENCE_KEY,
                THEME_LOCALE_CODE_DEFAULT_VALUE);
        updateLocale(context, localeCode);
    }

    public static void showDialog(Context context, VoidCallBack voidCallBack){
        String localeCode = HelperPref.get().getStringValueWithDefault(
                THEME_LOCALE_CODE_PREFERENCE_KEY,
                THEME_LOCALE_CODE_DEFAULT_VALUE);
        int defaultIndex = findIndex(localeCodes, localeCode, 0);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getText(R.string.timer_dialog_change_language_title))
                .setSingleChoiceItems(R.array.locale_entries, defaultIndex, (dialogInterface, i) -> {
                    String localeName = safeGetItem(localeCodes, i);
                    updateLocale(context, localeName);
                    HelperPref.get().putStringValue(THEME_LOCALE_CODE_PREFERENCE_KEY, localeName);
                })
                .setPositiveButton("OK", (dialog, id) -> {
                    dialog.dismiss();
                    voidCallBack.callback();
                })
                .setNegativeButton("Cancel", (dialog, id) -> dialog.dismiss());
        builder.create().show();
    }

    private static String[] getLocaleCodes(Context context) {
        String code = "";
        int signStartIndex = 0;
        int signEndIndex = 0;
        ArrayList<String> codes = new ArrayList<>();
        String[] entries = context.getResources().getStringArray(R.array.locale_entries);
        for (String entry: entries){
//            Log.d(TAG, "getLocaleCodes: " + entry);
            signStartIndex = entry.indexOf("-");
            signEndIndex = entry.lastIndexOf(")");
            if (signStartIndex != -1){
                code = entry.substring(signStartIndex + 1, signEndIndex);
                codes.add(code);
//                Log.d(TAG, "getLocaleCodes: " + code);
            }
        }
        return codes.toArray(new String[codes.size()]);
    }

    private static void updateLocale(Context context, String localeName) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        Configuration config = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(new Locale(localeName.toLowerCase()));
        } else {
            config.locale = new Locale(localeName.toLowerCase());
        }
        context.getResources().updateConfiguration(config, dm);
    }

    public static int findIndex(String[] arrays, String value, int defaultIndex){
        int i = 0;
        for (String item: arrays){
            if (item.equals(value)){
                return i;
            }
            i++;
        }
        return defaultIndex;
    }

    public static int findIndex(String[] arrays, String value){
        return findIndex(arrays, value, -1);
    }

    private static String safeGetItem(String[] array, int index) {
        try {
            return array[index];
        } catch (ArrayIndexOutOfBoundsException e) {
            return "en";
        }
    }

    private static String safeGetItem(String[] array, String localeCode) {
        for (String locale: array) {
            if (locale.equals(localeCode)){
                return localeCode;
            }
        }
        return "en";
    }

}
