package com.hamatim.navigation_ads_base;

public interface CallBack<DTO> {

    void callback(DTO dto);

}
