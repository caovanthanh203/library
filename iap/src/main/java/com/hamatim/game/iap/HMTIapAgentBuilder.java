package com.hamatim.game.iap;

import android.app.Application;

public interface HMTIapAgentBuilder {

    HMTIapAgentBuilder enableLimitTime(boolean value);

    HMTIapAgentBuilder enableDebug(boolean value);

    HMTIapAgentBuilder setPurchaseTimeToLiveInMilis(long value);

    HMTIapAgentBuilder setPendingPurchaseTimeToExpired(long value);

    HMTIapAgentBuilder setPendingPurchaseTimeToRecheck(long value);

    HMTIapAgentBuilder enableDryRemove(boolean value);

    HMTIapAgentBuilder addSku(String skuId);
}
