package com.hamatim.game.iap;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.hamatim.helper.HelperPref;
import com.hamatim.helper.VoidCallBack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.android.billingclient.api.Purchase.PurchaseState.PENDING;
import static com.android.billingclient.api.Purchase.PurchaseState.PURCHASED;

public class HMTIap implements HMTIapAgentBuilder, HMTIapAgent {

    private static final String TAG = "HMTIap";
    private static final String IAP_PAID_TIMESTAMP_PREFERENCE_KEY = "IAP_PAID_TIMESTAMP_PREFERENCE_KEY";
    private static final String IAP_PAID_TOKEN_PREFERENCE_KEY = "IAP_PAID_TOKEN_PREFERENCE_KEY";
    private static final String IAP_PAID_ORDER_ID_PREFERENCE_KEY = "IAP_PAID_ORDER_ID_PREFERENCE_KEY";
    private static final String IAP_PAID_ORDER_STATUS_PREFERENCE_KEY = "IAP_PAID_ORDER_STATUS_PREFERENCE_KEY";
    private static final String IAP_PAID_ACK_STATUS_PREFERENCE_KEY = "IAP_PAID_ACK_STATUS_PREFERENCE_KEY";

    private static final String IAP_PAID_SHOULD_RECHECK_PREFERENCE_KEY = "IAP_PAID_SHOULD_RECHECK_PREFERENCE_KEY";
    private static final String IAP_PAID_LAST_CHECK_TIMESTAMP_PREFERENCE_KEY = "IAP_PAID_LAST_CHECK_TIMESTAMP_PREFERENCE_KEY";
    private static HMTIapAgent instance;
    private static long timeToLiveInMillis = -1, timeToReCheckInMillis = 10000;
    private static boolean enableLimitTime = false;
    private static final List<String> skuList = new ArrayList<>();
    private BillingClient billingClient;
    private VoidCallBack successPurchaseCallback;
    private boolean enableDebug = false;
    private long timeToExpiredInMillis = 120000;
    private boolean dryRunRemove = false;

    public static void test(){
        HMTIap.get();
        if (enableLimitTime){
            if (timeToLiveInMillis < 0){
                throw new RuntimeException("timeToLiveInMillis is not set when enableLimitTime!!!");
            }
        }
    }

    private HMTIap(){

    }

    public static HMTIapAgent get(){
        if (null == instance) {
            throw new RuntimeException("You have to call HMTIap.init(applicationContext, applicationId) first!!!");
        }
        if (skuList.isEmpty()){
            throw new RuntimeException("You have to call HMTIap.addSku(skuId) first!!!");
        }
        return instance;
    }

    private static HMTIapAgentBuilder getInstance(){
        if (instance == null){
            instance = new HMTIap();
        }
        return (HMTIapAgentBuilder) instance;
    }

    public static HMTIapAgentBuilder init(Application application, String applicationId){
        HelperPref.init(application, applicationId);
        return getInstance();
    }

    @Override
    public HMTIapAgentBuilder enableLimitTime(boolean value){
        enableLimitTime = value;
        return (HMTIapAgentBuilder) get();
    }

    @Override
    public HMTIapAgentBuilder enableDebug(boolean value){
        enableDebug = value;
        return (HMTIapAgentBuilder) get();
    }

    @Override
    public HMTIapAgentBuilder setPurchaseTimeToLiveInMilis(long value){
        timeToLiveInMillis = value;
        return (HMTIapAgentBuilder) get();
    }

    @Override
    public HMTIapAgentBuilder setPendingPurchaseTimeToExpired(long value){
        timeToExpiredInMillis = value;
        return (HMTIapAgentBuilder) get();
    }

    @Override
    public HMTIapAgentBuilder setPendingPurchaseTimeToRecheck(long value){
        timeToReCheckInMillis = value;
        return (HMTIapAgentBuilder) get();
    }

    @Override
    public HMTIapAgentBuilder enableDryRemove(boolean value){
        dryRunRemove = value;
        return (HMTIapAgentBuilder) get();
    }

    @Override
    public HMTIapAgentBuilder addSku(String skuId){
        skuList.add(skuId);
        return (HMTIapAgentBuilder) get();
    }

    @Override
    public boolean checkSavedPurchase(Context context){
        return HMTIap.get().checkSavedPurchase(context, null, null, null);
    }

    @Override
    public boolean checkSavedPurchase(Context context, VoidCallBack havePurchased, VoidCallBack havePending, VoidCallBack notHave){
        log("isPurchasedCheckOffline");
        if (getSavedPurchaseToken().isEmpty()){ //not have token
            log("Not have any purchase saved");
            performCallback(notHave);
            return false; //not purchase yet, false to lite
        } else {
            //if order status is not complete
            if (!getSavedPurchaseStatus().equals("PURCHASED")){
                if (currentTime() - savedPurchaseTime() > timeToExpiredInMillis){
                    //remove purchase
                    log("Order status is not complete but expired, remove it");
                    removePurchase(context, getSavedPurchaseToken());
                    clearSavedPurchase();
                    performCallback(notHave);
                    return false; //purchase is decline, remove it, false to lite
                } else {
                    //if last check time not too short
                    if (currentTime() - getLastCheckTime() > timeToReCheckInMillis){
                        //recheck
                        log("Recheck pending purchase");
                        saveLastCheckTime(); //save new check time
                        validatePurchaseOnline(context);
                    } else {
                        log("Last check time too short");
                    }
                    performCallback(havePending);
                    return true; //not sure, so just keep premium
                }
            } else {
                //if not ack yet
                if (!isSavedPurchaseAckStatusComplete()){
                    //if last check time not too short
                    if (currentTime() - getLastCheckTime() > timeToReCheckInMillis) {
                        //try send ack request
                        log("Purchased not ack yet, begin send ack for purchase");
                        saveLastCheckTime(); //save new check time
                        simpleAcknowledgePurchase(context, getSavedPurchaseToken());
                    } else {
                        log("Last check ack time too short");
                    }
                }
                if (enableLimitTime){
                    if (isSavedPurchaseOutdate()){
                        log("Purchase is outdate, remove it");
                        removePurchase(context, getSavedPurchaseToken());
                        clearSavedPurchase();
                        performCallback(notHave);
                        return false; //purchase is outdate, remove it, false to lite
                    }
                }
                performCallback(havePurchased);
                return true; //purchase is good, keep premium
            }
        }
    }

    private boolean isSavedPurchaseOutdate() {
        long savedPurchasePaidTime = HelperPref.get().getLongValue(IAP_PAID_TIMESTAMP_PREFERENCE_KEY);
        return currentTime() - savedPurchasePaidTime > timeToLiveInMillis;
    }

    private void saveLastCheckTime() {
        HelperPref.get().putLongValue(IAP_PAID_LAST_CHECK_TIMESTAMP_PREFERENCE_KEY, currentTime());
    }

    private void validatePurchaseOnline(Context context) {
        startClientConnection(context, () -> {
            processAllPurchase(context, getUserPurchases(context));
        });
    }

    private void processAllPurchase(Context context, List<Purchase> userPurchases) {
        for (Purchase purchase: userPurchases){
            log("checking " + purchase.getOrderId());
            log("state " + purchase.getPurchaseState());
            if (!enableLimitTime || isPurchaseNotOutdate(purchase)){ //order not have limit or not outdate
                if (purchase.getPurchaseState() == PENDING){ //purchase is not complete
                    log(purchase.getOrderId() + " is PENDING");
                    if (currentTime() - purchase.getPurchaseTime() > timeToExpiredInMillis){  //but expired
                        log("remove " + purchase.getOrderId() + " because expired PENDING");
                        removePurchase(context, purchase.getPurchaseToken());
                    } else {
                        log("use " + purchase.getOrderId() + " because not expired PENDING, check later");
                        savePurchaseDetail(purchase);
                        break;
                    }
                } else {
                    log("use " + purchase.getOrderId() + " because is PURCHASED");
                    savePurchaseDetail(purchase);
                    if (!purchase.isAcknowledged()){
                        log("send ack " + purchase.getOrderId() + " because is PURCHASED but not ack");
                        silentAcknowledgePurchase(context, purchase);
                    }
                    break;
                }
            } else {
                log("Remove " + purchase.getOrderId() + " because outdate");
                removePurchase(context, purchase.getPurchaseToken());
            }
        }
    }

    private Purchase getFirstValidPurchase(Context context, List<Purchase> userPurchases) {
        Purchase anyPurchase = null;
        for (Purchase purchase: userPurchases){
            log("checking " + purchase.getOrderId());
            log("state " + purchase.getPurchaseState());
            if (!enableLimitTime || isPurchaseNotOutdate(purchase)){
                if (purchase.getPurchaseState() == PENDING){ //purchase is not complete
                    log(purchase.getOrderId() + " is PENDING");
                    if (currentTime() - purchase.getPurchaseTime() > timeToExpiredInMillis){  //but expired
                        log("remove " + purchase.getOrderId() + " because expired PENDING");
                        removePurchase(context, purchase.getPurchaseToken());
                    } else {
                        log("use " + purchase.getOrderId() + " because not expired PENDING, check later");
                        anyPurchase = purchase;
                        break;
                    }
                } else {
                    log("use " + purchase.getOrderId() + " because is PURCHASED");
                    anyPurchase = purchase;
                    if (!purchase.isAcknowledged()){
                        log("send ack " + purchase.getOrderId() + " because is PURCHASED but not ack");
                        silentAcknowledgePurchase(context, purchase);
                    }
                    break;
                }
            } else {
                log("Remove " + purchase.getOrderId() + " because outdate");
                removePurchase(context, purchase.getPurchaseToken());
            }
        }
        return anyPurchase;
    }

    private void silentAcknowledgePurchase(Context context, Purchase purchase) {
        acknowledgePurchase(context, purchase, true);
    }

    private boolean isPurchaseNotOutdate(Purchase purchase) {
        return currentTime() - purchase.getPurchaseTime() < timeToLiveInMillis;
    }

    private void clearSavedPurchase() {
        saveOrderAckStatus(false);
        HelperPref.get().putLongValue(IAP_PAID_TIMESTAMP_PREFERENCE_KEY, 0);
        HelperPref.get().putStringValue(IAP_PAID_TOKEN_PREFERENCE_KEY, "");
        HelperPref.get().putStringValue(IAP_PAID_ORDER_ID_PREFERENCE_KEY, "");
        HelperPref.get().putStringValue(IAP_PAID_ORDER_STATUS_PREFERENCE_KEY, "");
    }

    private boolean isSavedPurchaseAckStatusComplete() {
        return HelperPref.get().getBooleanValue(IAP_PAID_ACK_STATUS_PREFERENCE_KEY);
    }

    private long getLastCheckTime() {
        return HelperPref.get().getLongValue(IAP_PAID_LAST_CHECK_TIMESTAMP_PREFERENCE_KEY);
    }

    private long savedPurchaseTime() {
        return HelperPref.get().getLongValue(IAP_PAID_TIMESTAMP_PREFERENCE_KEY);
    }

    private long currentTime() {
        return System.currentTimeMillis();
    }

    private String getSavedPurchaseToken() {
        return HelperPref.get().getStringValueWithDefault(IAP_PAID_TOKEN_PREFERENCE_KEY, "");
    }

    private String getSavedPurchaseStatus() {
        return HelperPref.get().getStringValueWithDefault(IAP_PAID_ORDER_STATUS_PREFERENCE_KEY, "");
    }

    private BillingClient getBillingClient(Context context){
        if (billingClient == null){
            billingClient = BillingClient.newBuilder(context)
                    .setListener(getPurchaseUpdateListener(context))
                    .enablePendingPurchases()
                    .build();
        }
        return billingClient;
    }

    private void startClientConnection(Context context, VoidCallBack successConnected){
        getBillingClient(context).startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() ==  BillingClient.BillingResponseCode.OK) {
                    if (successConnected != null){
                        log("onBillingSetupFinished successConnected");
                        successConnected.callback();
                    }
                } else {
                    Toast.makeText(context, billingResult.getDebugMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onBillingServiceDisconnected() {
                //bypass
                log("onBillingServiceDisconnected");
            }
        });
    }

    @Override
    public void checkOnlinePurchase(Context context, VoidCallBack havePurchased, VoidCallBack havePending, VoidCallBack notHave){
        log("Start online check");
        startClientConnection(context, () -> {
            Purchase purchase = getFirstValidPurchase(context, getUserPurchases(context));
            if (purchase != null) {
                savePurchaseDetail(purchase);
                if (purchase.getPurchaseState() == PURCHASED){
                    performCallback(havePurchased);
                } else {
                    performCallback(havePending);
                }
            } else {
                clearSavedPurchase();
                performCallback(notHave);
            }
        });
    }

    @Override
    public void performPurchase(Context context, Activity activity, VoidCallBack successPurchaseCallback){
        log("Start purchase progress");
        this.successPurchaseCallback = successPurchaseCallback;
        startQueryPurchase(context, activity);
    }

    private List<Purchase> getUserPurchases(Context context){
        Purchase.PurchasesResult result = getBillingClient(context).queryPurchases(BillingClient.SkuType.INAPP);
        List<Purchase> purchases = new ArrayList<>();
        if (result.getPurchasesList() != null){
            purchases = result.getPurchasesList();
        }
        return purchases;
    }

    private void savePurchaseDetail(Purchase purchase) {
        log("------------------------------------------");
        log("Save purchase detail: ");
        log(purchase.getOriginalJson());
        log("------------------------------------------");
        String orderStatus = "PENDING";
        if (purchase.getPurchaseState() == PURCHASED){
            orderStatus = "PURCHASED";
        }
        saveOrderAckStatus(purchase.isAcknowledged());
        HelperPref.get().putLongValue(IAP_PAID_TIMESTAMP_PREFERENCE_KEY, purchase.getPurchaseTime());
        HelperPref.get().putStringValue(IAP_PAID_TOKEN_PREFERENCE_KEY, purchase.getPurchaseToken());
        HelperPref.get().putStringValue(IAP_PAID_ORDER_ID_PREFERENCE_KEY, purchase.getOrderId());
        HelperPref.get().putStringValue(IAP_PAID_ORDER_STATUS_PREFERENCE_KEY, orderStatus);
    }

    private void saveOrderAckStatus(boolean acknowledged) {
        log("Save ack status " + acknowledged);
        HelperPref.get().putBooleanValue(IAP_PAID_ACK_STATUS_PREFERENCE_KEY, acknowledged);
    }

    private void removePurchase(Context context, String purchaseToken) {
        if (dryRunRemove){
            log("Dry run remove purchase " + purchaseToken);
            return;
        }
        ConsumeParams consumeParams = ConsumeParams.newBuilder()
                        .setPurchaseToken(purchaseToken)
                        .build();
        ConsumeResponseListener listener = (billingResult, purchaseToken1) -> {
            //bypass
        };
        getBillingClient(context).consumeAsync(consumeParams, listener);
    }

    private void startQueryPurchase(Context context, Activity activity) {
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        getBillingClient(context).querySkuDetailsAsync(params.build(),
                (billingResult, skuDetailsList) -> {
                    log(billingResult.getDebugMessage());
                    log("Code billingResult " + billingResult.getResponseCode());
                    if (skuDetailsList != null) {
                        if (!skuDetailsList.isEmpty()) {
                            log("Sku detail list size " + skuDetailsList.size());
                            if (activity != null) {
                                startPurchaseFlow(context, skuDetailsList.get(0), activity);
                            }
                        }
                    } else {
                        log("Sku detail list null");
                    }
        });
    }

    private void startPurchaseFlow(Context context, SkuDetails skuDetails, Activity activity) {
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)
                .build();
        int responseCode = getBillingClient(context).launchBillingFlow(activity, billingFlowParams).getResponseCode();
    }

    private PurchasesUpdatedListener getPurchaseUpdateListener(Context context) {
        return (billingResult, purchases) -> {
            handleThePurchaseFlowResult(context, billingResult, purchases);
        };
    }

    private void handleThePurchaseFlowResult(Context context, BillingResult billingResult, List<Purchase> purchases) {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(context, purchase);
            }
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
        }
    }

    private void handlePurchase(Context context, Purchase purchase) {
        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            savePurchaseDetail(purchase);
            acknowledgePurchase(context, purchase, false);
        } else {
            addPurchaseRecheckSchedule();
            savePurchaseDetail(purchase);
            performCallback(successPurchaseCallback);
        }
    }

    private void removePurchaseRecheckSchedule() {
        log("removePurchaseRecheckSchedule");
        HelperPref.get().putBooleanValue(IAP_PAID_SHOULD_RECHECK_PREFERENCE_KEY, false);
        HelperPref.get().putLongValue(IAP_PAID_LAST_CHECK_TIMESTAMP_PREFERENCE_KEY, 0);
    }

    private void addPurchaseRecheckSchedule() {
        log("addPurchaseRecheckSchedule");
        HelperPref.get().putBooleanValue(IAP_PAID_SHOULD_RECHECK_PREFERENCE_KEY, true);
        HelperPref.get().putLongValue(IAP_PAID_LAST_CHECK_TIMESTAMP_PREFERENCE_KEY, 0);
    }

    private void simpleAcknowledgePurchase(Context context, String token){
        AcknowledgePurchaseParams acknowledgePurchaseParams =
                AcknowledgePurchaseParams.newBuilder()
                        .setPurchaseToken(token)
                        .build();
        startClientConnection(context, () -> {
            log("Send ack request");
            getBillingClient(context).acknowledgePurchase(acknowledgePurchaseParams, billingResult -> {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    saveOrderAckStatus(true);
                }
            });
        });
    }

    private void acknowledgePurchase(Context context, Purchase purchase, boolean silent){
        AcknowledgePurchaseParams acknowledgePurchaseParams =
                AcknowledgePurchaseParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();
        startClientConnection(context, () -> {
            log("Send ack request");
            getBillingClient(context).acknowledgePurchase(acknowledgePurchaseParams, billingResult -> {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    saveOrderAckStatus(true);
                    if (!silent) {
                        performCallback(successPurchaseCallback);
                    }
                }
            });
        });
    }

    private void performCallback(VoidCallBack callBack){
        if (callBack != null){
            callBack.callback();
        }
    }

    private void log(String msg){
        if (enableDebug) {
            Log.d(TAG, "Debug " + msg);
        }
    }

    @Override
    public String getOrderId() {
        return HelperPref.get().getStringValue(IAP_PAID_ORDER_ID_PREFERENCE_KEY);
    }

    @Override
    public long getOrderTimestamp() {
        return HelperPref.get().getLongValue(IAP_PAID_TIMESTAMP_PREFERENCE_KEY);
    }

    @Override
    public String getOrderStatus() {
        return HelperPref.get().getStringValue(IAP_PAID_ORDER_STATUS_PREFERENCE_KEY);
    }

    @Override
    public long getOrderTimeToLive() {
        return timeToLiveInMillis;
    }
}
