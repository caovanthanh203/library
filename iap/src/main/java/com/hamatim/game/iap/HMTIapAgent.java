package com.hamatim.game.iap;

import android.app.Activity;
import android.content.Context;

import com.hamatim.helper.VoidCallBack;

public interface HMTIapAgent {

    boolean checkSavedPurchase(Context context);

    boolean checkSavedPurchase(Context context, VoidCallBack havePurchased, VoidCallBack havePending, VoidCallBack notHave);

    void checkOnlinePurchase(Context context, VoidCallBack havePurchased, VoidCallBack havePending, VoidCallBack notHave);

    void performPurchase(Context context, Activity activity, VoidCallBack successPurchaseCallback);

    String getOrderId();

    long getOrderTimestamp();

    long getOrderTimeToLive();

    String getOrderStatus();
}
