package com.hamatim.fragment_menu;

import android.content.Context;
import android.view.View;
import androidx.annotation.NonNull;
import com.hamatim.fragment.AdapterBase;
import com.hamatim.fragment.VHBase;
import com.hamatim.fragment.ViewCallBack;

public class AdapterMenu extends AdapterBase<Menu, VHMenu> {

    private ViewCallBack<Menu> viewCallBack;

    public AdapterMenu(Context mContext) {
        super(mContext);
    }

    @Override
    protected VHMenu onCreateViewHolder(View root, int type) {
        VHMenu vhMenu = new VHMenu(root);
        vhMenu.setViewCallBack(getViewCallBack());
        return vhMenu;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.layout_item_menu_text_icon;
    }

//    @Override
//    public void onBindViewHolder(@NonNull VHMenu holder, int position) {
//        holder.setModel(getItem(position));
//    }

    public ViewCallBack<Menu> getViewCallBack() {
        return viewCallBack;
    }

    public void setViewCallBack(ViewCallBack<Menu> viewCallBack) {
        this.viewCallBack = viewCallBack;
    }

    @Override
    public void onBindViewHolder(@NonNull VHMenu holder, int position) {
        holder.setModel(getItem(position));
        holder.notifyDataSetChanged();
    }
}
