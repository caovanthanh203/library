package com.hamatim.fragment_menu;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.hamatim.fragment.VHBase;
import com.hamatim.fragment.ViewCallBack;
import com.hamatim.helper.HMTLogger;

public class VHMenu extends VHBase<Menu> {

    private static final String TAG = "VHMenu";
    private final TextView tvItemMenuName;
    private final ImageView imvItemMenuIcon;
    private ViewCallBack<Menu> viewCallBack;

    public VHMenu(@NonNull View itemView) {
        super(itemView);
        tvItemMenuName = itemView.findViewById(getTextViewNameResId());
        imvItemMenuIcon = itemView.findViewById(getImageViewNameResId());
        itemView.setOnClickListener(this::onClick);
    }

    protected int getTextViewNameResId() {
        return R.id.tvItemMenuName;
    }

    protected int getImageViewNameResId() {
        return R.id.imvItemMenuIcon;
    }

    private void onClick(View view) {
        if (getViewCallBack() != null){
            getViewCallBack().callback(view, getModel());
        }
    }

    @Override
    public void onDataSetChanged() {
        try {
            tvItemMenuName.setText(getModel().getName());
        } catch (Exception exception){
            HMTLogger.e(TAG, exception);
        }
        try {
            imvItemMenuIcon.setImageResource(getModel().getIconRes());
        } catch (Exception exception){
            HMTLogger.e(TAG, exception);
        }
    }

    public ViewCallBack<Menu> getViewCallBack() {
        return viewCallBack;
    }

    public void setViewCallBack(ViewCallBack<Menu> viewCallBack) {
        this.viewCallBack = viewCallBack;
    }
}
