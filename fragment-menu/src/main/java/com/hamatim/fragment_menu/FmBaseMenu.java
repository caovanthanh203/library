package com.hamatim.fragment_menu;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hamatim.fragment.FmBaseList;
import com.hamatim.helper.HelperLayout;

import java.util.ArrayList;
import java.util.List;

public abstract class FmBaseMenu extends FmBaseList<Menu, AdapterMenu> {

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_list;
    }

    @Override
    protected int getListViewId() {
        return R.id.rcvItemList;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected AdapterMenu onCreateAdapter(Context context) {
        return onCreateAdapter();
    }

    @Override
    protected AdapterMenu onCreateAdapter() {
        return new AdapterMenu(getContext());
    }

    @Override
    protected void postAdapterCreated(AdapterMenu adapter) {
        super.postAdapterCreated(adapter);
        adapter.setViewCallBack((view, menu) -> onMenuClick(menu));
        getAdapter().setmList(getMenuList());
        getAdapter().notifyDataSetChanged();
    }

    private void onMenuClick(Menu menu) {
        if (menu.isCallBackable()) {
            menu.getCallBack().callback();
        } else {
            navTo(menu.getActionRes());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(getListViewId());
        recyclerView.setLayoutManager(getLayoutManager());
        return view;
    }

    protected RecyclerView.LayoutManager getLayoutManager() {
        return HelperLayout.getDynamicGridLayoutManager(getContext(), getMaxSpan(), new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return getAdapter().getmList().get(position).getSpan();
            }
        });
    }

    protected int getMaxSpan() {
        return 4;
    }

    protected List<Menu> getMenuList() {
        List<Menu> menus = new ArrayList<>();
        menus.add(new Menu("Menu 1", null));
        menus.add(new Menu("Menu 2", null));
        return menus;
    }

}
