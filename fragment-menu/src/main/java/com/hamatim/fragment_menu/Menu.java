package com.hamatim.fragment_menu;

import com.hamatim.helper.VoidCallBack;

public class Menu {

    private String name;
    private int iconRes;
    private int backgroundColorRes;
    private int actionRes;
    private int viewType = -1;
    private int span = 1;
    private VoidCallBack callBack;

    public Menu(int viewType){
        this.viewType = viewType;
    }

    public Menu(String name, VoidCallBack voidCallBack, int viewType){
        this.name = name;
        this.callBack = voidCallBack;
        this.viewType = viewType;
    }

    public Menu(String name, VoidCallBack voidCallBack, int span, int viewType){
        this.name = name;
        this.callBack = voidCallBack;
        this.span = span;
        this.viewType = viewType;
    }

    public Menu(String name, VoidCallBack voidCallBack){
        this.name = name;
        this.callBack = voidCallBack;
    }

    public Menu(String name, int iconRes, int actionRes, int viewType) {
        this.name = name;
        this.iconRes = iconRes;
        this.actionRes = actionRes;
        this.viewType = viewType;
    }

    public Menu(String name, int iconRes, int actionRes, int span, int viewType) {
        this.name = name;
        this.iconRes = iconRes;
        this.actionRes = actionRes;
        this.span = span;
        this.viewType = viewType;
    }

    public Menu(String name, int iconRes, VoidCallBack callBack) {
        this.name = name;
        this.iconRes = iconRes;
        this.callBack = callBack;
    }

    public Menu(String name, int iconRes, VoidCallBack callBack, int viewType) {
        this.name = name;
        this.iconRes = iconRes;
        this.callBack = callBack;
        this.viewType = viewType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }

    public int getBackgroundColorRes() {
        return backgroundColorRes;
    }

    public void setBackgroundColorRes(int backgroundColorRes) {
        this.backgroundColorRes = backgroundColorRes;
    }

    public int getActionRes() {
        return actionRes;
    }

    public void setActionRes(int actionRes) {
        this.actionRes = actionRes;
    }

    public VoidCallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(VoidCallBack callBack) {
        this.callBack = callBack;
    }

    public boolean isCallBackable(){
        return getCallBack() != null;
    }

    public int getSpan() {
        return span;
    }

    public void setSpan(int span) {
        this.span = span;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
