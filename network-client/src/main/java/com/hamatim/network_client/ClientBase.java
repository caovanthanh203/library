package com.hamatim.network_client;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.hamatim.network_client.Tls12SocketFactory.enableAllTLS;

public class ClientBase {

    public static OkHttpClient getHttpsClient(boolean enableLog, boolean enableHttps){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if(enableLog) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);  // <-- this is the important line!
        }
        if(enableHttps) {
            enableAllTLS(httpClient);
        }
        return httpClient.build();
    }

}
