package com.hamatim.helper;

import android.database.Cursor;
import android.provider.Telephony;
import android.util.Log;

import com.hamatim.callback.HMTDTOCallBack;

public class HelperCursor {

    public static void readLineByLine(Cursor cursor, HMTDTOCallBack<Cursor> onNextLine){
        if (cursor != null) {
            int total = cursor.getCount();
            if (total > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        onNextLine.callback(cursor);
                    } while (cursor.moveToNext());
                }
            }
        }
    }

}
