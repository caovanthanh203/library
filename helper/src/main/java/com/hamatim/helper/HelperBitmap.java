package com.hamatim.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Type;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import ar.com.hjg.pngj.ImageLineByte;
import ar.com.hjg.pngj.ImageLineSetDefault;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;

import static com.hamatim.helper.HelperFile.getExternalStorageFolder;

public class HelperBitmap {

    private static final String TAG = "HelperBitmap";


    public static void processPng(Uri inputUri) {
        int loopCount = 0;
        int lineCount = 0;
        try {
            Allocation inAllocation = null;
            Allocation outAllocation = null;
            final int block_height = 64;
            HelperTime.pickTime("start " + block_height);
//            final int block_width = 64;

            FileInputStream orig = new FileInputStream(inputUri.getPath());
            FileInputStream orig2 = new FileInputStream(inputUri.getPath());

            File folder = getExternalStorageFolder("/");
            String fileName = HelperFile.getFileName(HelperFile.getFilePath(inputUri)) + "_processed.png";

            File file = new File(folder, fileName);
            FileOutputStream dest = new FileOutputStream(file);

            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(orig, false);
            Rect blockRect = new Rect();

            PngReader pngr = new PngReader(orig2);
            PngWriter pngw = new PngWriter(dest, pngr.imgInfo);
            pngw.copyChunksFrom(pngr.getChunksList());

            // keep compression quick
            pngw.getPixelsWriter().setDeflaterCompLevel(1);

            int channels = pngr.imgInfo.channels;; // needles to say, this should not be hardcoded
            int width = pngr.imgInfo.samplesPerRow / channels;
            int height = pngr.imgInfo.rows;

            pngr.close(); // don't need it anymore

            blockRect.left = 0;
            blockRect.right = width;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap blockBitmap;
            byte []bytes = new byte[width * block_height * 4];
            byte []byteline = new byte[width * channels];

            for (int row = 0; row <= height / block_height - 1; row++) {
                loopCount++;
                int h;

                // are we nearing the end?
                if((row + 1) *  block_height <= height)
                    h = block_height;
                else {
                    h = height - row * block_height;

                    // so that new, smaller Allocations are created
                    inAllocation = outAllocation = null;
                }

                blockRect.top = row * block_height;
                blockRect.bottom = row * block_height + h;

//                HelperTime.pickTime("blockRect");
                blockBitmap = decoder.decodeRegion(blockRect, options);
//                HelperTime.pickTime("decodeRegion");

//                if(inAllocation == null)
//                    inAllocation = Allocation..createFromBitmap(mRS, blockBitmap);
//
//                if(outAllocation == null)
//                {
//                    Type.Builder TypeDir = new Type.Builder(mRS, Element.U8_4(mRS));
//                    TypeDir.setX(width).setY(h);
//
//                    outAllocation = Allocation.createTyped(mRS, TypeDir.create());
//                }

//                inAllocation.copyFrom(blockBitmap);
//                mScript.forEach_saturation(inAllocation, outAllocation);
//                outAllocation = inAllocation;
                Paint paint = new Paint();
                paint.setColor(Color.RED);
                paint.setStrokeWidth(2);

                Bitmap outBitmap = blockBitmap.copy(Bitmap.Config.ARGB_8888, true);
                blockBitmap.recycle();

//                Log.d(TAG, "processPng: draw line");

//                Canvas canvas = new Canvas(outBitmap);
//                canvas.drawLine(0, 0, outBitmap.getWidth(), outBitmap.getHeight(), paint);
//                HelperTime.pickTime("drawLine");

//                inAllocation.copyTo(bytes);
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                outBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//                bytes = baos.toByteArray();

                int size = outBitmap.getRowBytes() * outBitmap.getHeight();
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                outBitmap.copyPixelsToBuffer(byteBuffer);
                bytes = byteBuffer.array();

                int byteIndex = 0;

                for(int raster = 0; raster < h; raster++) {
                    lineCount++;
                    for(int pixelIndex = 0; pixelIndex < width; pixelIndex++){
                        byteline[pixelIndex * channels] = bytes[byteIndex++];
                        byteline[pixelIndex * channels + 1] = bytes[byteIndex++];
                        byteline[pixelIndex * channels + 2] = bytes[byteIndex++];
                        byteline[pixelIndex * channels + 3] = bytes[byteIndex++];
                    }

                    ImageLineByte line = new ImageLineByte(pngr.imgInfo, byteline);
                    pngw.writeRow(line);
                }
            }
            pngw.end();
        } catch (IOException e)
        {
            Log.d("BIG", "File io problem");
        }
        HelperTime.pickTime("writeRow");
        Log.d(TAG, "processPng: looped " + loopCount);
        Log.d(TAG, "processPng: line " + lineCount);
    }

    public static Bitmap convert(String base64Str) throws IllegalArgumentException
    {
        byte[] decodedBytes = Base64.decode( base64Str.substring(base64Str.indexOf(",") + 1), Base64.DEFAULT );
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static String convert(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

}
