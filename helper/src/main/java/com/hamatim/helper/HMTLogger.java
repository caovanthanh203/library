package com.hamatim.helper;

import android.util.Log;

public class HMTLogger {
    private static final String TAG = "HMTLogger";
    private static boolean enable = true;

    public static void setEnable(boolean value){
        HMTLogger.enable = value;
    }

    public static void v(Object object, String message){
        v(object.getClass().getCanonicalName(), message);
    }

    public static void d(Object object, String message){
        d(object.getClass().getCanonicalName(), message);
    }

    public static void i(Object object, String message){
        i(object.getClass().getCanonicalName(), message);
    }

    public static void e(Object object, String message){
        e(object.getClass().getCanonicalName(), message);
    }

    public static void e(Object object, Exception e){
        e(object.getClass().getCanonicalName(), e);
    }

    public static void w(Object object, String message){
        w(object.getClass().getCanonicalName(), message);
    }

    public static void v(String value, String message){
        if (enable) {
            Log.v(TAG, value + ": " + message);
        }
    }


    public static void d(String value, String message){
        if (enable) {
            Log.d(TAG, value + ": " + message);
        }
    }

    public static void i(String value, String message){
        if (enable) {
            Log.i(TAG, value + ": " + message);
        }
    }

    public static void w(String value, String message){
        if (enable) {
            Log.w(TAG, value + ": " + message);
        }
    }

    public static void e(String value, String message){
        if (enable) {
            Log.e(TAG, value + ": " + message);
        }
    }

    public static void e(String value, Exception e){
        if (enable) {
            Log.e(TAG, value, e);
        }
    }

    public static void dBox(String TAG, String message){
        if (enable) {
            d(TAG, "==============================================");
            d(TAG, message);
            d(TAG, "==============================================");
        }
    }

}
