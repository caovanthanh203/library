package com.hamatim.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.hamatim.callback.HMTDTOCallBack;
import com.hamatim.callback.HMTViewCallBack;
import com.hamatim.callback.HMTVoidCallBack;

//import cn.pedant.SweetAlert.SweetAlertDialog;

public class HelperNotification {

    private static Toast toast;

    public static void askOkOrCancel(Context context, String title, String message, HMTVoidCallBack confirm){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", (dialog, index) -> {
                    if (confirm != null) {
                        confirm.callback();
                    }
                    dialog.dismiss();
                })
                .setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss())
                .show();

    }

    public static void askForOkOnly(Context context, String title, String message, HMTVoidCallBack confirm){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", (dialog, index) -> {
                    if (confirm != null) {
                        confirm.callback();
                    }
                    dialog.dismiss();
                })
                .setCancelable(false)
                .show();
    }

//    public static void showSuccess(Context context, String title, String message, HMTVoidCallBack confirm){
//        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
//                .setTitleText(title)
//                .setContentText(message)
//                .setConfirmText("OK")
//                .setConfirmClickListener(sDialog -> {
//                    if (confirm != null) {
//                        confirm.callback();
//                    }
//                    sDialog.dismiss();
//                })
//                .show();
//    }

    public static void showAnchorMenu(Context context, View anchor, int menuRes, HMTDTOCallBack<MenuItem> callBack){
        PopupMenu popupMenu = new PopupMenu(context, anchor);
        popupMenu.getMenuInflater().inflate(menuRes, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            callBack.callback(item);
            return true;
        });
        popupMenu.show();
    }

    private static void safeToast(Context context, String message, int length){
        try {
            toast.cancel();
        } catch (Exception exception){
            exception.printStackTrace();
        }
        try {
            toast = Toast.makeText(context, message, length);
            toast.show();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static void toastLong(Context context, String message){
        safeToast(context, message, Toast.LENGTH_LONG);
    }

    public static void toastShort(Context context, String message){
        safeToast(context, message, Toast.LENGTH_SHORT);
    }

    public static void showOptionMenu(Context context, HMTDTOCallBack<Integer> callback, String title, String... options){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());

        builder.setItems(options, (dialog, index) -> {
            callback.callback(index);
            dialog.dismiss();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
