package com.hamatim.helper;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class HelperView {

    public static void onClick(View root, int btId, VoidCallBack voidCallBack){
        try {
            View view = root.findViewById(btId);
            view.setOnClickListener(v -> voidCallBack.callback());
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static void onClick(Activity root, int btId, VoidCallBack voidCallBack){
        try {
            View view = root.findViewById(btId);
            view.setOnClickListener(v -> voidCallBack.callback());
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static String getText(View root, int btId){
        String text = "";
        try {
            EditText view = root.findViewById(btId);
            text = view.getText().toString();
        } catch (Exception exception){
            exception.printStackTrace();
        }
        return text;
    }

    public static void setText(View root, int id, String text){
        try {
            TextView textView = findView(root, id);
            textView.setText(text);
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static <V extends View> V findView(View root, int id){
        return root.findViewById(id);
    }

    public static void hide(View root, int id){
        try {
            View view = root.findViewById(id);
            view.setVisibility(View.INVISIBLE);
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static void gone(View root, int id){
        try {
            View view = root.findViewById(id);
            view.setVisibility(View.GONE);
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static void show(View root, int id){
        try {
            View view = root.findViewById(id);
            view.setVisibility(View.VISIBLE);
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

}
