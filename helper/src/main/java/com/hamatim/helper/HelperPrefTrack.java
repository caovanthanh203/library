package com.hamatim.helper;

import android.content.Context;
import android.util.Log;

import java.util.zip.CRC32;

public class HelperPrefTrack {
    private static final String TAG = "HelperPrefTrack";
    private static HelperPrefTrack instance;
    private final Context context;
    private static final String HELPER_TRACK_PREFIX_ = "HELPER_TRACK_PREFIX_";
    private static final String HELPER_COUNT_PREFIX_ = "HELPER_COUNT_PREFIX_";

    private HelperPrefTrack(Context context){
        HelperPref.test();
        this.context = context;
    }

    public static void test(){
        if (null == instance) {
            throw new RuntimeException("You have to call HelperPrefTrack.init(context) first!!!");
        }
    }

    public static HelperPrefTrack init(Context context){
        if (instance == null){
            instance = new HelperPrefTrack(context);
        }
        return instance;
    }

    public static HelperPrefTrack get() {
        test();
        return instance;
    }

    public void saveLastTime(String eventName){
        HelperPref.get().putLongValue(getTrackKey(eventName), System.currentTimeMillis());
    }

    public void clearLastTime(String eventName){
        HelperPref.get().putLongValue(getTrackKey(eventName), -1);
    }

    public long getLastTime(String eventName){
        return HelperPref.get().getLongValueWithDefault(getTrackKey(eventName), -1);
    }

    public boolean lastEventLateThan(String eventName, int hour, int minute, int second){
        long deltaTime = System.currentTimeMillis() - getLastTime(eventName);
        return deltaTime > hour*3600000 + minute*60000 + second*1000;
    }

    public boolean lastEventLateThan(String eventName, int minute, int second){
        long deltaTime = System.currentTimeMillis() - getLastTime(eventName);
        return deltaTime > minute*60000 + second*1000;
    }

    public boolean lastEventLateThan(String eventName, int second){
        long deltaTime = System.currentTimeMillis() - getLastTime(eventName);
        return deltaTime > second*1000;
    }

    public void increaseCountEvent(String eventName){
        increaseCountEventBy(eventName,1);
    }

    public void increaseCountEventBy(String eventName, int value){
        HelperPref.get().putLongValue(getCountKey(eventName), getCount(eventName) + value);
    }

    public long getCount(String eventName){
        return HelperPref.get().getLongValueWithDefault(getCountKey(eventName), 0);
    }

    public void resetCount(String eventName){
        HelperPref.get().putLongValue(getCountKey(eventName), 0);
    }

    private String getTrackKey(String eventName) {
        return HELPER_TRACK_PREFIX_ + eventName;
    }

    private String getCountKey(String eventName) {
        return HELPER_COUNT_PREFIX_ + eventName;
    }

}
