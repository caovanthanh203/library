package com.hamatim.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HelperList {


    public static int[] toIntArray(List<Integer> list)  {
        int[] ret = new int[list.size()];
        int i = 0;
        for (Integer e : list)
            ret[i++] = e;
        return ret;
    }

    public static <T> List<T> makeList(GenCallBack<T> callBack){
        List<T> mList = new ArrayList<T>();
        for (int i = 0; i < 100; i++){
            mList.add(callBack.callback(i));
        }
        return mList;
    }

    public static List<String> makeStringList(String... strings){
        return Arrays.asList(strings);
    }

    public static String[] getStrings(String... strings){
        return strings;
    }

}
