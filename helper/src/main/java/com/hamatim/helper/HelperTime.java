package com.hamatim.helper;

import android.util.Log;

public class HelperTime {
    private static final String TAG = "HelperTime";
    private static long startTime , endTime;

    public static void pickTime(String message){
        endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        startTime = endTime;
        Log.d(TAG, "=============================");
        Log.d(TAG, time + " ms -----> " + message);
        Log.d(TAG, "=============================");
    }

    public static void pickTime(){
        endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        startTime = endTime;
        Log.d(TAG, "=============================");
        Log.d(TAG, time + " ms");
        Log.d(TAG, "=============================");
    }

}
