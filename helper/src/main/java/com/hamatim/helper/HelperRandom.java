package com.hamatim.helper;

import java.util.Random;

public class HelperRandom {
    private final Random random = new Random();
    private static HelperRandom instance;

    public HelperRandom(){
        //construction, can be private
    }

    private static HelperRandom getInstance(){
        if (instance == null) {
            instance = new HelperRandom();
        }
        //if your instance have dynamic params, should be set here
        return instance;
    }

    private Random getRandom(){
        return random;
    }

    public void seed(long seed){
        getRandom().setSeed(seed);
    }

    public int privateRand(int bound){
        return getRandom().nextInt(bound);
    }

    public int privateRandInt(){
        return getRandom().nextInt();
    }

    public static int rand(int bound){
        return getInstance().privateRand(bound);
    }

    public static int randInt(){
        return getInstance().privateRandInt();
    }

    public static String randomFileName(String extension){
        return getInstance().privateRandInt() + extension;
    }

}
