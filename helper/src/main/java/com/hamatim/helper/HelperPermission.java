package com.hamatim.helper;

import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.content.ContextCompat;

public class HelperPermission {

    public static boolean allPermissionGranted(Context context, String[] permissions){
        for(String permission: permissions){
            if (!allPermissionGranted(context, permission)){
                return false;
            }
        }
        return true;
    }

    public static boolean allPermissionGranted(Context context, String permission){
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

}
