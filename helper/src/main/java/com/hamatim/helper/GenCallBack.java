package com.hamatim.helper;

public interface GenCallBack<DTO> {

    DTO callback(int position);

}
