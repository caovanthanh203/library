package com.hamatim.helper;

import android.graphics.Color;

import java.util.Random;

import static com.hamatim.helper.HelperRandom.rand;

public class HelperColor {

    private static HelperColor instance;

    private HelperColor(){
        //construction, can be private
    }

    private static HelperColor getInstance(){
        if (instance == null) {
            instance = new HelperColor();
        }
        //if your instance have dynamic params, should be set here
        return instance;
    }

    public static int genRGB(){
        return getInstance().genRGBHelper();
    }

    private int genRGBHelper(){
        return Color.rgb(rand(255), rand(255), rand(255));
    }

}
