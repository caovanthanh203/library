package com.hamatim.helper;

import android.content.Context;
import android.util.Log;

import com.hamatim.callback.HMTDTOCallBack;

import java.util.Calendar;
import java.util.TimeZone;

public class HMTAdsLimit {
    private static final String TAG = "HMTAdsLimit";
    private static HMTAdsLimit instance;
    private static boolean simulateLimit;
    private long clickLimit, secondLimit;
    private boolean enableDebug, enableFraudCheck;
    private HMTDTOCallBack<Long> fraudCallback;

    private HMTAdsLimit(){
        //construction, can be private
    }

    public static HMTAdsLimit init(){
        try {
            HelperPref.get();
        } catch (Exception exception) {
            throw new RuntimeException("You have to call HelperPref.init(applicationContext, stroreName) before HMTAdsLimit.init()!!!");
        }
        if (instance == null) {
            instance = new HMTAdsLimit();
        }

        get().clickLimit(2);
        get().secondLimit(30);
        get().enableFraudCheck(true);
        get().enableDebug(false);

        return HMTAdsLimit.get();
    }

    private static HMTAdsLimit get(){
        if (instance == null) {
            throw new RuntimeException("You have to call HMTAdsLimit.init() first!!!");
        }
        return instance;
    }

    private static long getCurrentDateInGMT(){
        Calendar mCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        return mCalendar.getTimeInMillis()/1000;
    }

    public static void trackClick(Context context){
        get().log("saveClickTime: " + HMTAdsLimit.getCurrentDateInGMT());
        if (get().passClickLimitTime(context)){
            //save first click time
            get().log("saveClickTime: save first click of new cycle");
            HelperPref.get().putLongValue(
                    context.getString(R.string.ads_fraud_strike_count_key), 0);
            HelperPref.get().putLongValue(
                    context.getString(R.string.ads_fraud_limit_click_time_key), getCurrentDateInGMT()
            );
            HelperPref.get().putLongValue(
                    context.getString(R.string.ads_fraud_limit_click_count_key), 1
            );
        } else {
            //increase click count
            get().log("saveClickTime: increase click of cycle");
            long clickCount = HelperPref.get().getLongValueWithDefault(
                    context.getString(R.string.ads_fraud_limit_click_count_key), 0
            ) + 1;
            HelperPref.get().putLongValue(
                    context.getString(R.string.ads_fraud_limit_click_count_key), clickCount
            );
        }
    }

    public static boolean isSafe(Context context) {
        if (get().isEnableFraudCheck()) {
            get().logBox("Fraud check is enabled");
            if (simulateLimit){
                get().logBox("clickFraudCheck: Simulate Fraud click detected!!!!!!");
                return false;
            }
            if (!get().passClickCount(context)) { //4 clicks
                if (!get().passClickLimitTime(context)) { //last click is 20s
                    get().logBox("clickFraudCheck: Fraud click detected!!!!!!");
                    long strikeCount = get().getFraudStrikeCount(context) + 1;
                    HelperPref.get().putLongValue(context.getString(R.string.ads_fraud_strike_count_key), strikeCount);
                    try {
                        get().logBox("Fraud callback!");
                        get().getFraudCallback().callback(strikeCount);
                    } catch (Exception ignore){

                    }
                    return false;
                }
            }
        } else {
            get().logBox("Fraud check is disabled");
        }
        return true;
    }


    protected void logBox(String message){
        get().log("clickFraudCheck: ==============================================");
        get().log(message);
        get().log("clickFraudCheck: ==============================================");
    }

    protected void log(String message){
        if (get().isEnableDebug()) {
            Log.d(TAG, message);
        }
    }

    private boolean passClickCount(Context context) {
        return getClickCount(context) < getClickLimit();
    }

    private long getClickCount(Context context) {
        return HelperPref.get().getLongValueWithDefault(
                context.getString(R.string.ads_fraud_limit_click_count_key), 0
        );
    }

    private boolean passClickLimitTime(Context context) {
        return getTimeFromLastClick(context) >= getSecondLimit();
    }

    private long getTimeFromLastClick(Context context) {
        return getCurrentDateInGMT() - getLastClickTime(context);
    }

    private long getLastClickTime(Context context) {
        return HelperPref.get().getLongValueWithDefault(
                context.getString(R.string.ads_fraud_limit_click_time_key), 0
        );
    }

    private long getFraudStrikeCount(Context context) {
        return HelperPref.get().getLongValueWithDefault(
                context.getString(R.string.ads_fraud_strike_count_key), 0
        );
    }

    public long getSecondLimit() {
        return secondLimit;
    }

    public HMTAdsLimit secondLimit(long secondLimit) {
        this.secondLimit = secondLimit;
        return get();
    }

    public long getClickLimit() {
        return clickLimit;
    }

    public HMTAdsLimit simulateLimit(boolean value) {
        this.simulateLimit = value;
        return get();
    }

    public HMTAdsLimit clickLimit(long clickLimit) {
        this.clickLimit = clickLimit;
        return get();
    }

    public boolean isEnableDebug() {
        return enableDebug;
    }

    public HMTAdsLimit enableDebug(boolean enableDebug) {
        this.enableDebug = enableDebug;
        return get();
    }

    public boolean isEnableFraudCheck() {
        return enableFraudCheck;
    }

    public HMTAdsLimit enableFraudCheck(boolean enableFraudCheck) {
        this.enableFraudCheck = enableFraudCheck;
        return get();
    }

    public HMTAdsLimit fraudCallback(HMTDTOCallBack<Long> voidCallBack){
        this.fraudCallback = voidCallBack;
        return get();
    }

    public HMTDTOCallBack<Long> getFraudCallback() {
        return fraudCallback;
    }
}
