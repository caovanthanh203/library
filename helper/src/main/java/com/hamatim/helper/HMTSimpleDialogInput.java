package com.hamatim.helper;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class HMTSimpleDialogInput extends Dialog {

    private TextView tvTitle, tvInputName, tvError;
    private DTOCallBack<String> callback;
    private HMTInputTextValidator textValidator;
    private EditText edtInput;
    private String title, inputName, inputValue;
    private boolean multiline = false;

    private HMTSimpleDialogInput(@NonNull Context context) {
        super(context);
    }

    public static HMTSimpleDialogInput create(Context context){
        return new HMTSimpleDialogInput(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRes());

        tvTitle = findViewById(R.id.tvTitle);
        tvError = findViewById(R.id.tvError);
        tvInputName = findViewById(R.id.tvInputName);
        edtInput = findViewById(R.id.edtInput);

        edtInput.setText(inputValue);
        edtInput.addTextChangedListener(getTextWatcher());

        tvTitle.setText(title);
        tvInputName.setText(inputName);

        findViewById(R.id.btCancel).setOnClickListener(v -> dismiss());
        findViewById(R.id.btSave).setOnClickListener(v -> {
            if (getTextValidator() != null){
                if (!getTextValidator().checkIfTextIsValid(edtInput.getText().toString())) {
                    tvError.setText(getTextValidator().textInvalidCause());
                    tvError.setVisibility(View.VISIBLE);
                    return;
                } else {
                    tvError.setVisibility(View.INVISIBLE);
                }
            }
            if (getCallback() != null){
                getCallback().callback(edtInput.getText().toString());
            }
            dismiss();
        });
    }

    private TextWatcher getTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (getTextValidator() != null){
                    if (!getTextValidator().checkIfTextIsValid(edtInput.getText().toString())) {
                        tvError.setText(getTextValidator().textInvalidCause());
                        tvError.setVisibility(View.VISIBLE);
                    } else {
                        tvError.setVisibility(View.INVISIBLE);
                    }
                }
            }
        };
    }

    public HMTSimpleDialogInput setTitle(String value){
        title = value;
        return this;
    }

    public HMTSimpleDialogInput setInputName(String value){
        inputName = value;
        return this;
    }

    public HMTSimpleDialogInput setInputDefaultValue(String value){
        inputValue = value;
        return this;
    }

    private int getLayoutRes() {
        if (multiline) {
            return R.layout.layout_dialog_add_multiline;
        }
        return R.layout.layout_dialog_add;
    }

    public DTOCallBack<String> getCallback() {
        return callback;
    }

    public HMTSimpleDialogInput setCallback(DTOCallBack<String> callback) {
        this.callback = callback;
        return this;
    }

    public HMTInputTextValidator getTextValidator() {
        return textValidator;
    }

    public HMTSimpleDialogInput setTextValidator(HMTInputTextValidator textValidator) {
        this.textValidator = textValidator;
        return this;
    }

    public HMTSimpleDialogInput enableMultiLine(boolean value) {
        this.multiline = value;
        return this;
    }
}
