package com.hamatim.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class HelperNews {

    private static final String TAG = "HelperNews";
    private static HelperNews instance;
    private Context context;
    private String title = "What's new?";
    private String message = "";
    private boolean enableDebug = false;
    private String HELPER_NEWS_LAST_VERSION_PREF_KEY = "HELPER_NEWS_LAST_VERSION_PREF_KEY";

    private HelperNews(Context context){
        HelperPref.test();
        this.context = context;
    }

    public static void test(){
        if (null == instance) {
            throw new RuntimeException("You have to call HelperNews.init(context) first!!!");
        }
    }

    public static HelperNews init(Context context){
        if (instance == null){
            instance = new HelperNews(context);
        }
        return instance;
    }

    public static HelperNews get() {
        test();
        return instance;
    }

    public HelperNews enableDebug(boolean value){
        this.enableDebug = value;
        return this;
    }

    public HelperNews setCustomPrefKey(String value){
        this.HELPER_NEWS_LAST_VERSION_PREF_KEY = value;
        return this;
    }

    public HelperNews setTitle(String value){
        this.title = value;
        return this;
    }

    public HelperNews setMessage(String value){
        this.message = value;
        return this;
    }

    public void showWhatNewsIfNeeded(Context context, int version){
        test();
        if (message.isEmpty()){
            throw new RuntimeException("You have to call HelperNews.setMessage(value) first!!!");
        }

        if (!isShownOnThisVersion(version)) {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("OK", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    })
                    .show();
        }
    }

    public boolean isShownOnThisVersion(int version){
        if (HelperPref.get().getIntValueWithDefault(HELPER_NEWS_LAST_VERSION_PREF_KEY, -1) != version){
            return false;
        } else {
            HelperPref.get().putIntValue(HELPER_NEWS_LAST_VERSION_PREF_KEY, version);
            return true;
        }
    }

    private void log(String message){
        if (enableDebug) {
            Log.d(TAG, "===================================");
            Log.d(TAG, "log: " + message);
            Log.d(TAG, "===================================");
        }
    }

}
