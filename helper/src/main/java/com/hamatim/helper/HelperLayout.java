package com.hamatim.helper;

import android.content.Context;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

public class HelperLayout {

    public static LinearLayoutManager getLayoutManager(Context context){
        return new LinearLayoutManager(context);
    }

    public static GridLayoutManager getDynamicGridLayoutManager(Context context, int maxSpan, GridLayoutManager.SpanSizeLookup spanSizeLookup){
        GridLayoutManager gridLayoutManager = getGridLayoutManager(context, maxSpan);
        gridLayoutManager.setSpanSizeLookup(spanSizeLookup);
        return gridLayoutManager;
    }

    public static GridLayoutManager getGridLayoutManager(Context context, int column){
        return new GridLayoutManager(context, column);
    }

    public static LinearLayoutManager getGridLayoutManager(Context context){
        return new GridLayoutManager(context, 2);
    }

    public static LinearLayout.LayoutParams getLinearDefaultLayoutParams(){
        return new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
    }

}
