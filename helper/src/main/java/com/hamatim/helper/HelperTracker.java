package com.hamatim.helper;

import android.content.Context;
import android.os.Bundle;

public class HelperTracker {

    public static Bundle createEvent(String name, String value){
        Bundle params = new Bundle();
        params.putString(name, value);
        return params;
    }

    public static Bundle createNavEvent(String from, String to){
        Bundle params = new Bundle();
        params.putString("from", from);
        params.putString("to", to);
        return params;
    }

    public static Bundle createSimpleEvent(String name){
        Bundle params = new Bundle();
        params.putString("name", name);
        return params;
    }

}
