package com.hamatim.helper;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class HelperPref {
    private static final String TAG = "HelperPref";
    private final SharedPreferences sharedPreferences;
    private static HelperPref instance;

    private HelperPref(Context context, String storeName) {
        sharedPreferences = context.getSharedPreferences(storeName, Context.MODE_PRIVATE);
    }

    private HelperPref(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static void createInstance(Context context, String storeName) {
        if (null == instance) {
            instance = new HelperPref(context, storeName);
        }
    }

    private static void createInstance(Context context) {
        if (null == instance) {
            instance = new HelperPref(context);
        }
    }

    private static HelperPref getInstance() {
        if (null == instance) {
            throw new RuntimeException("You have to call HelperPref.init(applicationContext, stroreName) first!!!");
        }
        return instance;
    }

    public static void test(){
        if (null == instance) {
            throw new RuntimeException("You have to call HelperPref.init(applicationContext, storeName) first!!!");
        }
    }

    public static void init(Application application, String applicationId) {
        createInstance(application, applicationId);
    }

    public static void init(Application application) {
        createInstance(application);
    }

    public static HelperPref get() {
        return getInstance();
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void setSharedPreferencesListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);
    }

    public void removeSharedPreferencesListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        getSharedPreferences().unregisterOnSharedPreferenceChangeListener(listener);
    }

    public SharedPreferences.Editor getSharedPreferencesEditor() {
        return getSharedPreferences().edit();
    }

    public Set<String> getStringSet(String key){
        Set<String> set = new HashSet<>();
        return getSharedPreferences().getStringSet(key, set);
    }

    public Integer getIntValue(String key){
        return getIntValueWithDefault(key, 1);
    }

    public Integer getIntValueWithDefault(String key, int value){
        return getSharedPreferences().getInt(key, value);
    }

    public Long getLongValue(String key){
        return getLongValueWithDefault(key, 1);
    }

    public double getDoubleValue(String key){
        return getDoubleValueWithDefault(key, "0");
    }

    public BigDecimal getBigDecimalValue(String key){
        return new BigDecimal(getStringValueWithDefault(key, "0"));
    }

    public void putBigDecimalValue(String key, BigDecimal bigDecimal){
        putStringValue(key, bigDecimal.toPlainString());
    }

    public double getDoubleValueWithDefault(String key, double value) {
        return Double.parseDouble(getStringValueWithDefault(key, String.valueOf(value)));
    }

    public double getDoubleValueWithDefault(String key, String value) {
        return Double.parseDouble(getStringValueWithDefault(key, value));
    }

    public void putDoubleValue(String key, double value){
        putStringValue(key, String.valueOf(value));
    }

    public Long getLongValueWithDefault(String key, long value){
        return getSharedPreferences().getLong(key, value);
    }

    public String getStringValue(String key){
        return getSharedPreferences().getString(key, "");
    }

    public String getStringValueWithDefault(String key, String value){
        return getSharedPreferences().getString(key, value);
    }

    public boolean getBooleanValue(String key){
        return getBooleanValue(key, true);
    }

    public boolean getBooleanValue(String key, boolean value){
        return getSharedPreferences().getBoolean(key, value);
    }

    public boolean valueInStringSet(String key, String value){
        Set<String> strings = getStringSet(key);
        if (strings.contains(value)){
            return true;
        } else {
            return false;
        }
    }

    public void addToStringSet(String key, String value){
        Set<String> strings = getStringSet(key);
        strings.add(value);
        putStringSet(key, strings);
    }

    public Set<String> removeFromStringSet(String key, String value){
        Set<String> strings = getStringSet(key);
        strings.remove(value);
        putStringSet(key, strings);
        return strings;
    }

    public void putStringSet(String key, Set<String> set){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.remove(key);
        editor.apply();
        editor.putStringSet(key, set);
        editor.apply();
    }

    public void putIntValue(String key, int value){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void putLongValue(String key, long value){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void putStringValue(String key, String value){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void putBooleanValue(String key, boolean value){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void remove(String key){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.remove(key);
        editor.apply();
    }

}
