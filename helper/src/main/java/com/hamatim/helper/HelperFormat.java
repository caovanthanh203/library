package com.hamatim.helper;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HelperFormat {
    private static final String TAG = "HelperFormat";
    private static SimpleDateFormat simpleDateFormat;
    private static boolean showSecond;

    public static String formatAsDateTime(long timestamp){
        return getSimpleDateTimeFormat().format(new Date(timestamp));
    }

    public static String formatAsDateTimeFileName(long timestamp){
        return getSimpleDateTimeFormatForFile().format(new Date(timestamp));
    }

    private static SimpleDateFormat getSimpleDateTimeFormatForFile() {
        return new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss", Locale.getDefault());
    }

    public static String format(Date submitDate) {
        if (submitDate != null){
            return getSimpleDateFormat().format(submitDate);
        }
        return "--/--/--";
    }

    public static String formatHour(Date submitDate) {
        if (submitDate != null){
            return getSimpleHourFormat().format(submitDate);
        }
        return "--:--:--";
    }

    public static void changeHourMode(boolean value){
        showSecond = value;
        simpleDateFormat = null;
    }

    public static String formatFloat(double value, int decimal){
        String template = "%,." + decimal + "f";
        return String.format(Locale.US, template, Math.abs(value));
    }

    public static String formatInt(int value){
        String template = "%,d";
        return String.format(Locale.US, template, value);
    }

    private static SimpleDateFormat getSimpleHourFormat() {
        if (simpleDateFormat == null){
            Log.d(TAG, "getSimpleHourFormat: " + showSecond);
            if (showSecond) {
                simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            } else {
                simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            }
        }
        return simpleDateFormat;
    }

    public static SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    }

    public static SimpleDateFormat getSimpleDateTimeFormat() {
        if (simpleDateFormat == null){
            simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        }
        return simpleDateFormat;
    }

    public static boolean getHourMode() {
        return showSecond;
    }
}
