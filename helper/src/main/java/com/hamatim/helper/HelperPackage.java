package com.hamatim.helper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

import java.io.File;
import java.util.List;
import java.util.Locale;

public class HelperPackage {

    public static File getPackageApkFile(Context context, String packageName) throws PackageManager.NameNotFoundException {
        String apkPath = getApplicationInfo(context, packageName).publicSourceDir;
        File apkFile = new File(apkPath);
        if (apkFile.exists()){
            if (apkFile.canRead()){
                return apkFile;
            }
        }
        return null;
    }

    public static ApplicationInfo getApplicationInfo(Context context, String packageName) throws PackageManager.NameNotFoundException {
        return getPackageInfo(context, packageName).applicationInfo;
    }

    public static PackageInfo getPackageInfo(Context context, String packageName) throws PackageManager.NameNotFoundException {
        PackageManager packageManager = context.getPackageManager();
        return packageManager.getPackageInfo(packageName, 0);
    }

    public static List<PackageInfo> getPackageList(Context context){
        PackageManager packageManager = context.getPackageManager();
        return packageManager.getInstalledPackages(0);
    }

    public static void openStorePage(Context context, String packageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (Exception exception) {
            exception.printStackTrace();
            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
            } catch (Exception childException) {
                childException.printStackTrace();
                Toast.makeText(context, "Can not open store page!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static boolean isFirstInstall(Context context) {
        try {
            long firstInstallTime = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
            long lastUpdateTime = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
            return firstInstallTime == lastUpdateTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return true;
        }
    }

    /**
     * Shares the PNG image from Uri.
     * @param uri Uri of image to share.
     */
    public static void shareImageUri(Context context, Uri uri){
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/png");
        context.startActivity(intent);
    }

    public static void sharePackage(Context context, String packageId){
        if (context == null){
            return;
        }

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, String.format(Locale.US, "https://play.google.com/store/apps/details?id=%s", packageId));
        intent.setType("text/plain");
        context.startActivity(intent);
    }

}
