package com.hamatim.helper;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import static android.content.Context.CLIPBOARD_SERVICE;

public class HelperClipboard {

    public static String paste(Context context){
        String value = "";
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        if (clipboardManager.hasPrimaryClip()){
            try {
                value = clipboardManager.getPrimaryClip().getItemAt(0).getText().toString();
            } catch (Exception exception){
                exception.printStackTrace();
            }
        }
        return value;
    }

    public static void copy(Context context, String value){
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        try {
            clipboardManager.setPrimaryClip(ClipData.newPlainText("text", value));
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

}
