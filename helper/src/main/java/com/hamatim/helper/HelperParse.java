package com.hamatim.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HelperParse {

    public static double parseDouble(String value){
        try {
            return Double.parseDouble(value);
        } catch (Exception excaption){
            return 0.0d;
        }
    }

    public static int parseInteger(String value){
        try {
            return Integer.parseInt(value);
        } catch (Exception excaption){
            return 0;
        }
    }

    public static Date parseDateTime(String date) {
        try {
            return HelperFormat.getSimpleDateTimeFormat().parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date parseDateTime(String date, SimpleDateFormat format) {
        try {
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
