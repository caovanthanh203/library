package com.hamatim.helper;

import android.app.Activity;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;

public abstract class HelperActivity {

    public static void show(Activity context, Class activityClass, boolean clearStack){
        if (clearStack) {
            context.finish();
        }
        context.startActivity(new Intent(context, activityClass));
    }

    public static void show(Activity context, Class activityClass){
        show(context, activityClass, true);
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ignore){}
    }

}
