package com.hamatim.helper;

public interface DTOCallBack<DTO> {

    void callback(DTO dto);

}
