package com.hamatim.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.FileUtils;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.util.Random;

import static com.hamatim.helper.HelperNotification.toastLong;
import static org.slf4j.MDC.put;

public class HelperFile {
    private static final String TAG = "HelperFile";

    public static String genFileName(String prefix){
        return prefix + System.currentTimeMillis();
    }

    public static String genFileName(String prefix, String ext){
        return prefix + System.currentTimeMillis() + ext;
    }

    public static File getFileFromUri(Uri uri) {
        return new File(uri.getPath());
    }

    public static FileDescriptor getFileDescriptorFromUri(Context context, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        parcelFileDescriptor.close();
        return fileDescriptor;
    }

    public static Bitmap getBitmapFromUri(Context context, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static Intent getExternalPickFileIntent(String type) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(
                getUriFromPath(
                        getExternalStoragePath("/")
                ),
                type
        );

        return Intent.createChooser(intent, getTitle());
    }

    public static Intent getPickFileIntent(String type) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.setType(type);
        return Intent.createChooser(intent, getTitle());
    }

    private static CharSequence getTitle() {
        return "Choose file";
    }

    private static String getExternalStoragePath(String path) {
        return Environment.getExternalStorageDirectory() + path;
    }


    public static String getExDownloadPath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
    }

    public static String getStorageImagePath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
    }

    public static String getExDownloadPath(String filename) {
        return getExDownloadPath() + "/" + filename ;
    }

    public static String getFilePath(Uri uri) {
        String path = uri.getPath();
        if (path == null)
            return null;
        path =  path.replace("/document/raw:", "");
        return path;
    }

    public static File getCacheFolder(Context context) {
        return context.getCacheDir();
    }

    public static File getExternalStorageFolder(String path) {
        return new File(getExternalStoragePath(path));
    }

    public static File getDownloadFolder(String path) {
        return new File(getExternalStoragePath(path));
    }

    public static Uri getUriFromPath(String path) {
        return Uri.parse(path);
    }

    public static String saveImage(String filename, Bitmap finalBitmap) {
        File folder = getExternalStorageFolder("/");
        String fileName = filename + ".png";

        File file = new File(folder, fileName);
        if (file.exists())
            file.delete();

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.v("saving", fileName);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return folder + "/" + fileName;
    }

    public static String saveImageToTemp(String filename, Bitmap finalBitmap) {
        String fileName = filename + ".png";
        File file = null;
        try {
            file = File.createTempFile(fileName, ".png");
            file.deleteOnExit();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.v("saving", fileName);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "saveImageToTemp: " + file.getAbsolutePath());

        return file.getAbsolutePath();
    }

    public static File saveImageToTempFile(Bitmap finalBitmap) {
        String fileName = "temp.png";
        File file = null;
        try {
            file = File.createTempFile(fileName, ".png");
            file.deleteOnExit();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            HMTLogger.v("saving", fileName);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        HMTLogger.d(TAG, "saveImageToTemp: " + file.getAbsolutePath());

        return file;
    }

    public static String saveImageTo(File folder, String filename, Bitmap finalBitmap) {
        return saveImageTo(folder.getPath(), filename, finalBitmap);
    }

    public static String saveImageTo(String folderPath, String filename, Bitmap finalBitmap) {
        File folder = new File(folderPath);
        String fileName = filename;

        File file = new File(folder, fileName);
        if (file.exists())
            file.delete();

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.v("saving", fileName);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return folder + "/" + file;
    }

    public static File saveImageToFile(String folderPath, String filename, Bitmap finalBitmap) {
        File folder = new File(folderPath);
        String fileName = filename;

        File file = new File(folder, fileName);
        if (file.exists())
            file.delete();

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            Log.v("saving", fileName);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    private static ContentValues contentValues() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000);
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        return values;
    }

    public static String copyFileToExDownload(Context context, File file){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return saveFileUsingMediaStore(context, file);
        } else {
            return copyFileTo(getExDownloadPath(), file.getName(), file);
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private static String saveFileUsingMediaStore(Context context, File file) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, file.getName());
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "*/*");
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS);
        Uri uri = context.getContentResolver().insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);
        if (uri != null) {
            copyFileTo(context, file, uri);
            return Environment.DIRECTORY_DOWNLOADS + "/" + file.getName();
        }
        return "";
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    public static String copyFileTo(Context context, File file, Uri newFileUri) {
//        if (newFile.exists())
//            newFile.delete();

        try {
            FileChannel inputChannel = null;
            FileChannel outputChannel = null;
            try {
                inputChannel = new FileInputStream(file).getChannel();
                outputChannel = ((FileOutputStream) context.getContentResolver().openOutputStream(newFileUri)).getChannel();
                outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
            } finally {
                inputChannel.close();
                outputChannel.close();
            }
//            Log.d(TAG, "copyFileTo: " + newFile.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file.getName();
    }

    public static String copyFileTo(String folderPath, String newFileName, File file) {
        File folder = new File(folderPath);
        File newFile = new File(folder, newFileName);
//        if (newFile.exists())
//            newFile.delete();

        try {
            FileChannel inputChannel = null;
            FileChannel outputChannel = null;
            try {
                inputChannel = new FileInputStream(file).getChannel();
                outputChannel = new FileOutputStream(newFile).getChannel();
                outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
            } finally {
                inputChannel.close();
                outputChannel.close();
            }
            Log.d(TAG, "copyFileTo: " + newFile.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "copyFileTo: You should check if storage permission is granted?");
        }

        return folder + "/" + newFileName;
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            FileUtils.copy(inputStream, outputStream);
        } else {
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0) {
                    outputStream.write(buf, 0, len);
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static String getFileName(String path) {
        if (path == null)
            return null;

        int index = path.lastIndexOf("/");
        return index < path.length() ? path.substring(index + 1) : null;
    }

    public static boolean delete(String filePath) {
        File file = new File(filePath);

        if (file.exists()){
            Log.d(TAG, "delete: " + getFileName(filePath));
            file.delete();
        }

        return !file.exists();
    }

    /**
     * Saves the image as PNG to the app's cache directory.
     * @param image Bitmap to save.
     * @return Uri of the saved file or null
     */
    public static Uri saveImage(Context context, Bitmap image) {
        //TODO - Should be processed in another thread
        File imagesFolder = new File(context.getCacheDir(), "images");
        Uri uri = null;
        try {
            imagesFolder.mkdirs();
            File file = new File(imagesFolder, genFileName("image_", ".png"));
            file.deleteOnExit();

            FileOutputStream stream = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.flush();
            stream.close();
            uri = FileProvider.getUriForFile(context, "com.hamatim.pixelstudio.fileprovider", file);

        } catch (IOException e) {
            Log.d(TAG, "IOException while trying to write file for sharing: " + e.getMessage());
        }
        return uri;
    }

    public static boolean rename(String filePath, String fileName) {
        File file = new File(filePath);
        File newFile = new File(file.getParent(), fileName);
        file.renameTo(newFile);

        if (newFile.exists()){
            Log.d(TAG, "renamed to: " + getFileName(newFile.getAbsolutePath()));
            file.delete();
        }

        return !file.exists();
    }

    public static void notifyImageChanged(Context context){
        context.getContentResolver().notifyChange(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null);
        context.getContentResolver().notifyChange(MediaStore.Images.Media.INTERNAL_CONTENT_URI, null);
    }

    public static void notifyImageAdded(Context context, File file){
        ContentValues values = contentValues();
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
//            values.put(MediaStore.Images.Media.RELATIVE_PATH, getStorageImagePath());
//            values.put(MediaStore.Images.Media.IS_PENDING, true);
//            // RELATIVE_PATH and IS_PENDING are introduced in API 29.
//
//            Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
//            if (uri != null) {
//                saveImageToStream(bitmap, context.contentResolver.openOutputStream(uri));
//                values.put(MediaStore.Images.Media.IS_PENDING, false)
//                context.contentResolver.update(uri, values, null, null)
//            }
//        }

        values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
//        values.put(String.valueOf(MediaStore.Images.Media.EXTERNAL_CONTENT_URI), file.getAbsolutePath());

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

//    public static void notifyImageChanged(Context context, String path){
//        context.sendBroadcast(
//                new Intent(
//                        Intent.ACTION_MEDIA_MOUNTED,
//                        Uri.parse(path)
//                )
//        );
//    }

}
