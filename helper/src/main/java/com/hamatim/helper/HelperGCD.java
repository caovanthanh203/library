package com.hamatim.helper;

import java.util.List;

public class HelperGCD {

	// Function to return gcd of a and b
	public static int gcd(int a, int b)
	{
		if (a == 0)
			return b;
		return gcd(b % a, a);
	}

	// Function to find gcd of array of
	// numbers
	public static int gcd(int[] arr)
	{
		int result = 0;
		for (int element: arr){
			result = gcd(result, element);

			if(result == 1)
			{
			return 1;
			}
		}

		return result;
	}

}

