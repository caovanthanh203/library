package com.hamatim.helper;

public interface HMTInputTextValidator {

    boolean checkIfTextIsValid(String text);
    String textInvalidCause();

}
