package com.hamatim.helper;

import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.Telephony;
import android.util.Log;

public class HelperCallLog {

    private static final String TAG = "HelperCallLog";

    public static final int INCOMING_TYPE = 0;
    public static final int OUTGOING_TYPE = 1;
    public static final int MISSED_TYPE = 2;

    public static final String INCOMING_TYPE_NAME = "incoming";
    public static final String OUTGOING_TYPE_NAME = "outgoing";
    public static final String MISSED_TYPE_NAME = "missed";

    public static int getCallLogType(String name) {
        int callType = 0;
        switch (name) {
            case INCOMING_TYPE_NAME:
                callType = CallLog.Calls.INCOMING_TYPE;
                break;
            case OUTGOING_TYPE_NAME:
                callType = CallLog.Calls.OUTGOING_TYPE;
                break;
            case MISSED_TYPE_NAME:
                callType = CallLog.Calls.MISSED_TYPE;
                break;
            default:
                Log.e(TAG, "The callType is not defined. Defaulting it to value corresponds to INCOMING_TYPE. Please check: callType = " + callType);
                callType = CallLog.Calls.INCOMING_TYPE;
        }
        return callType;
    }

    public static int getCallLogType(int type) {
        int callType = 0;
        switch (type) {
            case INCOMING_TYPE:
                callType = CallLog.Calls.INCOMING_TYPE;
                break;
            case OUTGOING_TYPE:
                callType = CallLog.Calls.OUTGOING_TYPE;
                break;
            case MISSED_TYPE:
                callType = CallLog.Calls.MISSED_TYPE;
                break;
            default:
                Log.e(TAG, "The callType is not defined. Defaulting it to value corresponds to INCOMING_TYPE. Please check: callType = " + callType);
                callType = CallLog.Calls.INCOMING_TYPE;
        }
        return callType;
    }

    public static int deleteAll(Context context){
        return context.getContentResolver().delete(CallLog.Calls.CONTENT_URI, null, null);
    }

    public static int getCount(Context context){
        final String[] projector = new String[] {Telephony.Sms._ID};
        Cursor countCursor = context.getContentResolver().query(
                CallLog.Calls.CONTENT_URI,
                projector,
                null,
                null,
                null);
        return countCursor.getCount();
    }

    public static void dumpCursor(Cursor cursor){
        if (cursor != null){
            for (String column: cursor.getColumnNames()){
                Log.d(TAG, "dumpCursor: " + column);
//                int index = cursor.getColumnIndex(column);

//                if (index != -1) {
//                    Log.d(TAG, "dumpCursor: " + cursor.getType(0));
//                }
            }
        }
    }

}
