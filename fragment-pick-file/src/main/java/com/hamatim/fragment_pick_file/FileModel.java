package com.hamatim.fragment_pick_file;

import java.io.File;

public class FileModel {

    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}
