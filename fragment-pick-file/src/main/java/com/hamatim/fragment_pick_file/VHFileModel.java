package com.hamatim.fragment_pick_file;

import android.media.ThumbnailUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.hamatim.fragment.VHBase;
import com.hamatim.fragment.ViewCallBack;

public class VHFileModel extends VHBase<FileModel> {
    private TextView tvFileName;
    private ViewCallBack<FileModel> viewCallBack;
    private ImageView imvFileThumb;
    private boolean showRoot;

    public VHFileModel(@NonNull View itemView) {
        super(itemView);
        tvFileName = itemView.findViewById(R.id.tvFileName);
        imvFileThumb = itemView.findViewById(R.id.imvFileThumb);
        itemView.setOnClickListener(view -> {
            try {
                getViewCallBack().callback(view, getModel());
            } catch (Exception exception){
                exception.printStackTrace();
            }
        });
    }

    @Override
    public void onDataSetChanged() {
        try {
            if (getAdapterPosition() == 0 && showRootPosition()){
                tvFileName.setText("..");
            } else {
                tvFileName.setText(getModel().getFile().getName());
            }
            if (getModel().getFile().isDirectory()){
                imvFileThumb.setImageResource(R.drawable.ic_baseline_folder_24);
            } else {
                imvFileThumb.setImageResource(R.drawable.ic_baseline_file_24);
            }
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public void showRootPosition(boolean showRoot){
        this.showRoot = showRoot;
    }

    public boolean showRootPosition(){
        return this.showRoot;
    }

    public ViewCallBack<FileModel> getViewCallBack() {
        return viewCallBack;
    }

    public void setViewCallBack(ViewCallBack<FileModel> viewCallBack) {
        this.viewCallBack = viewCallBack;
    }
}
