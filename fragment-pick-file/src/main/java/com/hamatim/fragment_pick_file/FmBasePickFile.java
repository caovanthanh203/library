package com.hamatim.fragment_pick_file;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.hamatim.fragment.FmBaseList;
import com.hamatim.helper.HelperFile;
import com.hamatim.helper.HelperLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class FmBasePickFile extends FmBaseList<FileModel, AdapterFileModel> {
    private static final String TAG = "FmBasePickFile";
    private File currentFolder;

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return HelperLayout.getLayoutManager(getContext());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_list;
    }

    @Override
    protected int getListViewId() {
        return R.id.rcvItemList;
    }

    @Override
    protected AdapterFileModel onCreateAdapter() {
        return new AdapterFileModel(getContext());
    }

    @Override
    protected void postAdapterCreated(AdapterFileModel adapter) {
        super.postAdapterCreated(adapter);
        adapter.setViewCallBack(((view, data) -> {
            if (data.getFile().isDirectory()) {
                if (pickFolder()) {
                    Log.d(TAG, "postAdapterCreated: pick folder");
                    pick(data.getFile());
                } else {
                    changeFolder(data.getFile());
                }
            } else {
                if (pickFile() || (!pickFile() && !pickFolder())) {
                    pick(data.getFile());
                }
            }
        }));
    }

    private void pick(File file) {
        Log.d(TAG, "pickFile: " + file.getPath());
        Log.d(TAG, "pickFile: " + file.getAbsolutePath());
        Uri uri = Uri.fromFile(file);
        boolean isFolder = file.isDirectory();
        onFilePicked(uri, isFolder);
    }

    /**
     * @deprecated use {@link }
     * @param uri
     */
    @Deprecated
    protected void onFilePicked(Uri uri){

    };

    protected void onFilePicked(Uri uri, boolean isFolder){
        onFilePicked(uri);
    };

    protected boolean pickFolder(){
        return false;
    }

    protected boolean pickFile(){
        return false;
    }

    private void changeFolder(File file) {
        Log.d(TAG, "changeFolder: " + file.getAbsolutePath());
        setCurrentFolder(file);

        List<FileModel> fileModelList = new ArrayList<>();
        FileModel fileModel;

        for (File mFile : file.listFiles()){
            if (mFile.isDirectory()) {
                String[] fileList = mFile.list();
                if (fileList != null && fileList.length > 0) {
                    fileModel = new FileModel();
                    fileModel.setFile(mFile);
                    fileModelList.add(fileModel);
                }
            } else {
                fileModel = new FileModel();
                fileModel.setFile(mFile);
                fileModelList.add(fileModel);
            }
        };

        fileModel = new FileModel();
        fileModel.setFile(file.getParentFile());

        fileModelList.add(0, fileModel);
        getAdapter().setmList(fileModelList);
        getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        changeFolder(getStartDirectory());
    }

    protected File getStartDirectory(){
        return Environment.getRootDirectory();
    };

    public File getCurrentFolder() {
        return currentFolder;
    }

    public void setCurrentFolder(File file) {
        this.currentFolder = file;
    }
}
