package com.hamatim.fragment_pick_file;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.hamatim.fragment.AdapterBase;
import com.hamatim.fragment.ViewCallBack;

public class AdapterFileModel extends AdapterBase<FileModel, VHFileModel> {
    private ViewCallBack<FileModel> viewCallBack;

    public AdapterFileModel(Context mContext) {
        super(mContext);
    }

    @Override
    protected VHFileModel onCreateViewHolder(View root, int viewType) {
        VHFileModel vhFileModel = new VHFileModel(root);
        vhFileModel.setViewCallBack(getViewCallBack());
        return vhFileModel;
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return R.layout.layout_item_file;
    }

    @Override
    public void onBindViewHolder(@NonNull VHFileModel holder, int position) {
        holder.showRootPosition(position == 0 && showRootPosition());
        holder.setModel(getItem(position));
        holder.onDataSetChanged();
    }

    public boolean showRootPosition(){
        return true;
    }

    public ViewCallBack<FileModel> getViewCallBack() {
        return viewCallBack;
    }

    public void setViewCallBack(ViewCallBack<FileModel> viewCallBack) {
        this.viewCallBack = viewCallBack;
    }

}
