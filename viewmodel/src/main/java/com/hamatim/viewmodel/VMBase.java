package com.hamatim.viewmodel;

import android.app.Activity;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class VMBase<DTO> extends ViewModel{

    private MutableLiveData<List<DTO>> modelListMutableLiveData;
    private MutableLiveData<DTO> modelMutableLiveData;

    public VMBase() {
        modelMutableLiveData = new MutableLiveData<>();
        modelMutableLiveData.postValue(null);

        modelListMutableLiveData = new MutableLiveData<>();
        modelListMutableLiveData.postValue(new ArrayList<>());
    }

    public void watchModel(Fragment fragment, Observer<? super DTO> observer){
        watchModel(fragment.getViewLifecycleOwner(), observer);
    }

    public void watchModel(LifecycleOwner lifecycleOwner, Observer<? super DTO> observer){
        modelMutableLiveData.observe(lifecycleOwner, observer);
    }

    public void watchModelList(Fragment fragment, Observer<? super List<DTO>> observer){
        watchModelList(fragment.getViewLifecycleOwner(), observer);
    }

    public void watchModelList(LifecycleOwner lifecycleOwner, Observer<? super List<DTO>> observer){
        modelListMutableLiveData.observe(lifecycleOwner, observer);
    }

    protected List<DTO> getModelList(){
        return modelListMutableLiveData.getValue();
    }

    public void setModelList(List<DTO> modelList){
        modelListMutableLiveData.postValue(modelList);
    }

    protected DTO getModel(){
        return modelMutableLiveData.getValue();
    }

    public void setModel(DTO model){
        modelMutableLiveData.postValue(model);
    }

}
