package com.hamatim.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.HashMap;

public class ProviderVMBase {
    private static final String TAG = "ProviderVMBase";
    private ViewModelProvider.AndroidViewModelFactory viewModelProvider;

    private static ProviderVMBase instance;
    private static HashMap<String, Object> cached;

    protected ProviderVMBase(){
        //construction, can be private
    }

    protected static ProviderVMBase getInstance(){
        if (instance == null) {
            instance = new ProviderVMBase();
        }
        return instance;
    }

    protected static <T extends ViewModel> T getVM(final Class<T> service) {
        if (null == getCache().get(service.getCanonicalName())) {
            getCache().put(service.getCanonicalName(), createVM(service));
        }
        return (T) getCache().get(service.getCanonicalName());
    }

    private static <T extends ViewModel> T createVM(Class<T> tClass){
        try {
            return getInstance().getViewModelProvider().create(tClass);
        } catch (Exception exception){
            Log.e(TAG, "createVM: ======================================");
            Log.e(TAG, "createVM: ========android:name=\"<application_class>\" missing?==========");
            Log.e(TAG, "createVM: ========VMProvider.init(applicationContext) missing?===========");
            exception.printStackTrace();
            throw new RuntimeException("Can not init " + tClass.getName() + ", read previous log!");
        }
    }

    public static void init(Application application){
        getInstance().setViewModelProvider(
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        );
    }

    private static HashMap<String, Object> getCache() {
        if (cached == null){
            cached = new HashMap<>();
        }
        return cached;
    }


    private ViewModelProvider.AndroidViewModelFactory getViewModelProvider() {
        if (viewModelProvider == null){
            if (null == instance) {
                throw new RuntimeException("You have to call ProviderVM.init(applicationContext) first!!!");
            }
        }
        return viewModelProvider;
    }

    protected void setViewModelProvider(ViewModelProvider.AndroidViewModelFactory viewModelProvider) {
        this.viewModelProvider = viewModelProvider;
    }

}
