package com.hamatim.database;

import androidx.annotation.NonNull;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public abstract class EntityBase {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    @NonNull
    protected String id;

    public EntityBase() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void generateNewId(){
        setId(UUID.randomUUID().toString());
    }

}
