package com.hamatim.database;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.hamatim.helper.VoidCallBack;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class DAOBase<Model extends EntityBase> {

    private List<VoidCallBack> voidCallBackList = new ArrayList<>();

    public void notifyDatabaseUpdated() {
        voidCallBackList.removeAll(Collections.singleton(null));
        for (VoidCallBack voidCallBack: voidCallBackList){
            if (voidCallBack != null){
                voidCallBack.callback();
            }
        }
    }

    public void addDatachangedListener(VoidCallBack voidCallBack){
        voidCallBackList.add(voidCallBack);
    }

    public void removeDatachangedListener(VoidCallBack voidCallBack){
        voidCallBackList.remove(voidCallBack);
    }

    @RawQuery
    public List<Model> readAll() {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("Select * From " + getTableName());
        return doQuery(query);
    }

    @RawQuery
    public List<Model> readById(String id) {
        return readByField("id", id);
    }

    @RawQuery
    public List<Model> readByField(String fieldName, String value) {
        String queryString = "Select * from %s where %s = '%s'";
        queryString = String.format(queryString, getTableName(), fieldName, value);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryString);
        return doQuery(query);
    }

    @RawQuery
    public Model readFirst() {
        String queryString = "Select * from %s limit 1";
        queryString = String.format(queryString, getTableName());
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryString);
        return doQueryFirst(query);
    }

    @RawQuery
    public Model readFirst(String id) {
        return readFirst(id, "id");
    }

    @RawQuery
    public Model readFirst(String value, String fieldName) {
        String queryString = "Select * from %s where %s = '%s' limit 1";
        queryString = String.format(queryString, getTableName(), fieldName, value);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryString);
        return doQueryFirst(query);
    }

    protected String getTableName() {
        throw new RuntimeException("Not implement getTableName() yet!");
    }

    @RawQuery
    public List<Model> deleteAll() {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("Delete From " + getTableName());
        return doQuery(query);
    }

    @RawQuery
    public abstract List<Model> doQuery(SupportSQLiteQuery query);

    @RawQuery
    public abstract Model doQueryFirst(SupportSQLiteQuery query);

    @Insert
    public abstract void insert(Model model);

    @Insert
    public abstract void insert(List<Model> models);

    @Update
    public abstract void update(Model model);

    @Update
    public abstract void update(List<Model> models);

    @Delete
    public abstract void delete(Model model);

    @Delete
    public abstract void delete(List<Model> models);

}
