package com.hamatim.hmt_ads;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.hamatim.helper.HMTAdsLimit;
import com.hamatim.hmt_base.ActivityBase;

public abstract class ActivityBaseAds extends ActivityBase {
    private static final String TAG = "ActivityBaseAds";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initAdsView();

        if (shouldServeAds()) {
            initAdSDK();
        } else {
            hideAds();
        }
    }

    private boolean shouldServeAds() {
        if (forceNotLoadAds()){ //is force to hide
            Log.e(TAG, "loadPersonalizedAds: ==============================================");
            Log.e(TAG, "loadPersonalizedAds: forceNotLoadAds!!!!!!");
            Log.e(TAG, "loadPersonalizedAds: ==============================================");
            return false;
        }

        if (shouldCheckFraud()) {
            if (!HMTAdsLimit.isSafe(this)) {
                return false;
            }
        }

        if (!HelperAds.isAdsReadyToServe(this)){
            String dateString = String.format(
                    "%s %s %s",
                    getString(R.string.ads_ready_date),
                    getString(R.string.ads_ready_time),
                    getString(R.string.ads_ready_zone)
            );
            Log.e(TAG, "loadPersonalizedAds: ==============================================");
            Log.e(TAG, "loadPersonalizedAds: Not ready to serve ads!!!!!!");
            Log.e(TAG, "loadPersonalizedAds: Wait to " + dateString);
            Log.e(TAG, "loadPersonalizedAds: ==============================================");
            return false;
        }


        if (!isFreeWithAdsUser()){ //is pro
            Log.e(TAG, "loadPersonalizedAds: ==============================================");
            Log.e(TAG, "loadPersonalizedAds: User is premium, not load ads!!!!!!");
            Log.e(TAG, "loadPersonalizedAds: ==============================================");
            return false;
        }
        return true;
    }

    protected boolean isFreeWithAdsUser() {
        return true;
    }

    protected boolean shouldCheckFraud(){
        return false;
    };

    protected void initAdsView() {

    }

    protected boolean testAdsWithRealId(){
        if (forceTestAdsWithRealId()) {
            Log.e(TAG, "testAdsWithRealId: ==============================================");
            Log.e(TAG, "testAdsWithRealId: Remember to turn on test ads on networks!!!!!!");
            Log.e(TAG, "testAdsWithRealId: ==============================================");
        }
        return forceTestAdsWithRealId() || HelperAds.hasTestWithRealAdsKey(getPackageManager());
    }

    protected boolean testAdsWithFakeId(AdRequest adRequest) {
        return HelperAds.hasTestKey(getPackageManager()) || adRequest.isTestDevice(this);
    }

    protected boolean forceTestAdsWithRealId(){
        return false;
    };

    protected boolean forceNotLoadAds(){
        return false;
    };

    private void initAdSDK(){
//        Log.d(TAG, "initAdSDK: " + getString(getLive));

        if (getString(R.string.live_admob_application_id).equals(getString(R.string.test_admob_application_id))){
            Log.e(TAG, "initAdSDK: ==============================================");
            Log.e(TAG, "initAdSDK: Remember to override @string/live_admob_application_id at production ads!!!!!!");
            Log.e(TAG, "initAdSDK: ==============================================");
        }

        if (getLiveAdsUnitId().equals(getFakeAdsUnitId())){
            Log.e(TAG, "initAdSDK: ==============================================");
            Log.e(TAG, "initAdSDK: Remember to override @string/live_admob_unit_id at production ads!!!!!!");
            Log.e(TAG, "initAdSDK: ==============================================");
        }

        if (testAdsWithRealId()){
            Toast.makeText(this, "TEST_ADS_WITH_REAL_ID ENABLED", Toast.LENGTH_LONG).show();
            HelperAds.initTestDevice(this);
        }

//        MobileAds.initialize(this, initializationStatus -> onSdkInitDone());
        //by pass because it not fire the callback from 9th Nov 2021
        MobileAds.initialize(this, initializationStatus -> {});
        loadAds();
    }



    protected void loadAds(){
        loadAdmobAds(getAdRequest());
    }

    protected AdRequest getAdRequest() {
        return new AdRequest.Builder().build();
    }

    private void loadAdmobAds(AdRequest adRequest) {
        if (testAdsWithRealId()){
            loadAdsWithRealUnit(adRequest);
        } else {
            if (testAdsWithFakeId(adRequest)) {
                Log.d("Ads", "This is test device");
                loadAdsWithFakeUnit(adRequest);
            } else {
                loadAdsWithRealUnit(adRequest);
            }
        }
    }

    protected void hideAds(){

    };

    protected abstract void loadAdsWithFakeUnit(AdRequest adRequest);

    protected abstract void loadAdsWithRealUnit(AdRequest adRequest);

    protected void onSdkInitDone(){
        loadAds();
    };

    protected String getLiveAdsUnitId(){
        return getString(R.string.live_admob_unit_id);
    };

    protected String getFakeAdsUnitId(){
        return getString(R.string.test_admob_unit_id);
    };

}
