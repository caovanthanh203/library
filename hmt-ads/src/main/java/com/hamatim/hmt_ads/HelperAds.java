package com.hamatim.hmt_ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.hamatim.helper.HelperPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;

public class HelperAds {
    private static final String TAG = "HelperAds";

    @SuppressLint("SimpleDateFormat")
    private static SimpleDateFormat getDateFormater(Context context) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(getDatePattern(context));
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        return dateFormat;
    }

    private static String getDatePattern(Context context) {
        return context.getString(R.string.ads_ready_date_gmt_pattern);
    }

    private static String getAdsReadyDateString(Context context) {
        return String.format(
                "%s %s %s",
                context.getString(R.string.ads_ready_date),
                context.getString(R.string.ads_ready_time),
                context.getString(R.string.ads_ready_zone)
        );
    }

    public static long getDateAdsReadyInGMT(Context context){
        Date date = new Date();
        try {
            date = getDateFormater(context).parse(getAdsReadyDateString(context));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime()/1000;
    }

    public static long getCurrentDateInGMT(){
        Calendar mCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        return mCalendar.getTimeInMillis()/1000;
    }

    public static boolean isAdsReadyToServe(Context context){
        return dateIsReady(context);
    }

    private static long getLastClickTime(Context context) {
        return HelperPref.get().getLongValueWithDefault(
                context.getString(R.string.ads_fraud_limit_click_time_key), 0
        );
    }

    public static boolean dateIsReady(Context context) {
        return (getCurrentDateInGMT() - getDateAdsReadyInGMT(context)) > 0;
    }

    public static boolean hasTestKey(PackageManager packageManager) {
        String targetPackage = "com.hamatim.this_is_test_device";
        try {
            PackageInfo info = packageManager.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static boolean hasTestWithRealAdsKey(PackageManager packageManager) {
        String targetPackage = "com.hamatim.this_is_test_device_force_real_ads";
        try {
            PackageInfo info = packageManager.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    @SuppressLint("HardwareIds")
    public static void initTestDevice(Context context){
        String deviceHash = getDeviceIdHash(context);
        Log.d(TAG, "initTestDevice: " + deviceHash);
        RequestConfiguration configuration;
        configuration = new RequestConfiguration.Builder().setTestDeviceIds(Collections.singletonList(deviceHash)).build();
        MobileAds.setRequestConfiguration(configuration);
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceIdHash(Context context){
        String testDevice = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return hash(testDevice);
    }

    public static String hash(String text) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(text.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException ignored) {
        }
        return null;
    }

}
