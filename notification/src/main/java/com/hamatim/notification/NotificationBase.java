package com.hamatim.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;

import androidx.core.app.NotificationCompat;

public abstract class NotificationBase {

    private Context context;
    private NotificationManager notificationManager;

    public NotificationBase(Context context){
        //construction, can be private
        this.context = context;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannel();
    }

    public void showNotification(String title, String message){
        Notification notification =
                new NotificationCompat.Builder(context, getChannelId())
                        .setSmallIcon(getSmallIconDrawable())
                        .setContentTitle(title)
                        .setContentText(message)
                        .setContentIntent(getPendingIntent())
                        .build();
        if (notificationManager != null){
            notificationManager.notify(getNotificationId(), notification);
        }
    }

    private void createNotificationChannel(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mainNotificationChannel = new NotificationChannel(getChannelId(),
                    getChannelName(), NotificationManager.IMPORTANCE_LOW);
            mainNotificationChannel.setDescription(getChannelDesc());
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mainNotificationChannel);
            }
        }
    }

    protected abstract int getNotificationId();

    protected abstract String getChannelDesc();

    protected abstract CharSequence getChannelName();

    protected abstract PendingIntent getPendingIntent();

    protected abstract int getSmallIconDrawable();

    protected abstract String getChannelId();

}
