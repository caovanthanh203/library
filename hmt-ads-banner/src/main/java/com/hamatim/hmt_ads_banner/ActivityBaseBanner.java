package com.hamatim.hmt_ads_banner;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.hamatim.helper.HMTAdsLimit;
import com.hamatim.hmt_ads.ActivityBaseAds;

public abstract class ActivityBaseBanner extends ActivityBaseAds {
    private static final String TAG = "ActivityBaseBanner";
    private ProgressBar adLoading;

    private AdView mAdView;
    private AdView testAdview;
    private FrameLayout adContainerView;
    private RelativeLayout adContainer;
    private LinearLayout adContainerWrapper;
//    private View divider;

    @Override
    protected void initAdsView() {
        adContainer = findViewById(R.id.adContainer);
        adLoading = findViewById(R.id.adLoading);
        adContainerView = findViewById(R.id.adViewContainer);
        adContainerWrapper = findViewById(R.id.adContainerWrapper);

        notNull(adContainer);
        notNull(adLoading);
        notNull(adContainerView);
        notNull(adContainerWrapper);
//        divider = findViewById(R.id.divider);

        adContainer.setVisibility(View.VISIBLE);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_activity_base_banner;
    }

    @Override
    protected void loadAdsWithFakeUnit(AdRequest adRequest) {
        testAdview = new AdView(this);
        testAdview.setVisibility(View.GONE);
        testAdview.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d(TAG, "onAdLoaded: ");
//                setDivider(View.VISIBLE);
                adContainerWrapper.setVisibility(View.VISIBLE);
                adContainer.setVisibility(View.VISIBLE);
                adLoading.setVisibility(View.GONE);
                testAdview.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Log.d(TAG, "onAdFailedToLoad: ");
                testAdview.setVisibility(View.GONE);
                hideAds();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                Log.d(TAG, "onAdClicked: ");
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
                Log.d(TAG, "onAdImpression: ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.d(TAG, "onAdClosed: ");
                if (shouldCheckFraud()) {
                    if (!HMTAdsLimit.isSafe(ActivityBaseBanner.this)) {
                        hideAds();
                    }
                }
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                if (shouldCheckFraud()) {
                    HMTAdsLimit.trackClick(ActivityBaseBanner.this);
                }
//                Log.d(TAG, "onAdOpened: ");
            }
        });
        Log.d(TAG, "loadBannerAdsFakeUnit: " + getFakeAdsUnitId());
        testAdview.setAdUnitId(getFakeAdsUnitId());
        adContainerView.removeAllViews();
        adContainerView.addView(testAdview);
        testAdview.setAdSize(getAdSize());
        testAdview.loadAd(adRequest);
    }

//    protected void setDivider(int visible) {
//        if (divider != null) {
//            divider.setVisibility(visible);
//        }
//    }

    public void loadAdsWithRealUnit(AdRequest adRequest){
        mAdView = new AdView(this);
        mAdView.setVisibility(View.GONE);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
//                setDivider(View.VISIBLE);
                adContainerWrapper.setVisibility(View.VISIBLE);
                adContainer.setVisibility(View.VISIBLE);
                adLoading.setVisibility(View.GONE);
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                mAdView.setVisibility(View.GONE);
                hideAds();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
//                Log.d(TAG, "onAdClicked: ");
            }

            @Override
            public void onAdImpression() {
                super.onAdImpression();
//                Log.d(TAG, "onAdImpression: ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
//                Log.d(TAG, "onAdClosed: ");
                if (shouldCheckFraud()) {
                    if (!HMTAdsLimit.isSafe(ActivityBaseBanner.this)) {
                        hideAds();
                    }
                }
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                if (shouldCheckFraud()) {
                    HMTAdsLimit.trackClick(ActivityBaseBanner.this);
                }
//                Log.d(TAG, "onAdOpened: ");
            }
        });
        Log.d(TAG, "loadBannerAdsRealUnit: " + getLiveAdsUnitId());
        mAdView.setAdUnitId(getLiveAdsUnitId());
        adContainerView.removeAllViews();
        adContainerView.addView(mAdView);
        mAdView.setAdSize(getAdSize());
        mAdView.loadAd(adRequest);
    }

    protected void hideAds() {
        if (testAdview != null) {
            testAdview.destroy();
            testAdview = null;
        }

        if (mAdView != null) {
            mAdView.destroy();
            mAdView = null;
        }

        adLoading.setVisibility(View.GONE);
        adContainer.setVisibility(View.GONE);
        adContainerWrapper.setVisibility(View.GONE);
//        setDivider(View.GONE);

        adContainer.removeAllViews();
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

}
