package com.hamatim.fragment;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.hamatim.helper.HelperView;

public class VHString extends VHBase<String> {

    public static int layout = R.layout.layout_item_text;

    public VHString(@NonNull View itemView) {
        super(itemView);
    }

    @Override
    public void onDataSetChanged() {
        HelperView.setText(itemView, R.id.tvItemName, getModel());
    }
}
