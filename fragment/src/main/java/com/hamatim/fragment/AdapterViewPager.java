package com.hamatim.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.List;

public class AdapterViewPager extends FragmentStateAdapter {

    private List<? super FmBase> fragmentlist;
    private List<String> fragmentListName;
    private TabLayoutMediator tabLayoutMediator;

    public AdapterViewPager(@NonNull Fragment fragment) {
        super(fragment);
    }

    public void setFragmentlist(List<? super FmBase> fragmentlist) {
        this.fragmentlist = fragmentlist;
    }

    public void setFragmentListName(List<String> fragmentListName) {
        this.fragmentListName = fragmentListName;
    }

//    public TabLayoutMediator getTabLayoutMediator() {
//        return tabLayoutMediator;
//    }

//    public void attachMediator(TabLayout tabLayout, ViewPager2 viewPager) {
//        tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
//            tab.setText(getPageTitle(position));
//            viewPager.setCurrentItem(tab.getPosition(), true);
//        });
//    }
//
//    public void dettachMediator(){
//        tabLayoutMediator = null;
//    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return (Fragment) fragmentlist.get(position);
    }

    @Override
    public int getItemCount() {
        return fragmentlist.size();
    }

}
