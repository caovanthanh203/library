package com.hamatim.fragment;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class VHBaseWrapper extends RecyclerView.ViewHolder {

    public VHBaseWrapper(@NonNull View itemView) {
        super(itemView);
    }

    protected <T extends View> T getView(int id){
        return itemView.findViewById(id);
    }

}
