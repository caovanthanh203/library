package com.hamatim.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.preference.PreferenceFragmentCompat;

import com.hamatim.helper.HelperView;
import com.hamatim.helper.VoidCallBack;
import com.hamatim.hmt_base.ActivityBase;
import com.hamatim.navigation.ActivityBaseGraph;

public abstract class FmBaseConfig extends PreferenceFragmentCompat {

    private static final String TAG = "FmBaseConfig";
    protected abstract int getPrefResId();

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the preferences from an XML resource
        setPreferencesFromResource(getPrefResId(), rootKey);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    protected void initView(View root){

    }

    protected void onClick(int id, VoidCallBack voidCallBack){
        HelperView.onClick(getView(), id, voidCallBack);
    }

    protected String getViewText(int id){
        return HelperView.getText(getView(), id);
    }

    protected void setViewText(int id, String text){
        HelperView.setText(getView(), id, text);
    }

    protected void hide(int id){
        HelperView.hide(getView(), id);
    }

    protected void show(int id){
        HelperView.show(getView(), id);
    }

    protected void gone(int id){
        HelperView.show(getView(), id);
    }

    protected String getTitle(){
        return getNavTitle();
    }

    protected void setTitle(String title){
        ActivityBase activity = null;
        try {
            activity = (ActivityBase) getActivity();
        } catch (Exception ignore){}
        if (activity != null) {
            activity.setTitle(title);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!getTitle().isEmpty()){
            ActivityBase activity = null;
            try {
                activity = (ActivityBase) getActivity();
            } catch (Exception ignore){}
            if (activity != null) {
                activity.setTitle(getTitle());
            }
        }
        initWatcher();
    }

    protected void initWatcher(){};

    protected String getNavTitle(){
        try {
            return getNavController().getCurrentDestination().getLabel().toString();
        } catch (Exception exception){
            Log.e(TAG, "getNavTitle: ", exception);
            showDebugToast(exception.getMessage());
        }
        return getClass().getName();
    }

    protected void showToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    protected void showDebugToast(String message){
        if (!disableDebugToast()) {
            showToast(message);
        }
    }

    protected boolean disableDebugToast() {
        return false;
    }

    protected NavController getNavController(){
        return Navigation.findNavController(getActivity(), getNavControllerResId());
    }

    protected int getNavControllerResId(){
        try {
            return getNavControllerResIdFromActivity();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    };

    protected int getNavControllerResIdFromActivity() throws Exception {
        ActivityBaseGraph activity = null;
        try {
            activity = (ActivityBaseGraph) getActivity();
        } catch (Exception ignore){
        }
        if (activity != null) {
            return activity.getHostFragmentResId();
        } else {
            throw new Exception("Only use on ActivityBaseGraph or need override getNavControllerResId");
        }
    };

    protected void navTo(int action){
        try {
            getNavController().navigate(action);
        } catch (Exception exception){
            Log.e(TAG, "navTo: ", exception);
            showDebugToast(exception.getMessage());
        }
    }

    protected void navTo(int action, Bundle bundle){
        try {
            getNavController().navigate(action, bundle);
        } catch (Exception exception){
            Log.e(TAG, "navTo: ", exception);
            showDebugToast(exception.getMessage());
        }
    }

    protected Bundle createBundle(){
        return new Bundle();
    }

    protected void navBack(){
        try {
            getNavController().popBackStack();
        } catch (Exception exception){
            Log.e(TAG, "navTo: ", exception);
            showDebugToast(exception.getMessage());
        }
    }

}
