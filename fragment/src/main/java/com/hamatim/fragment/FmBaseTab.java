package com.hamatim.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import com.google.android.material.tabs.TabLayout;

import java.util.Arrays;
import java.util.List;

public abstract class FmBaseTab extends FmBase {
    private static final String TAG = "FmBaseTab";
    private TabLayout tlaTab;
    private NavController navController;
    private Fragment navHostFragment;

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_base_tab;
    }

    protected void initGraph(View root){
        try {
            getNavController(root).setGraph(getNavigationGraphResId());
            navController.addOnDestinationChangedListener(this::onNavChangeListener);
        } catch (Resources.NotFoundException exception){
            exception.printStackTrace();
            Log.d(TAG, "initGraph: Does getNavigationGraphResId return valid id?\nEx: R.navigation.your_graph");
        }
    }

    protected NavController getNavController(View root){
        if (navController == null){
            navController = Navigation.findNavController(getNavHostFragment().getView());
        }
        return navController;
    }

    protected NavController getChildNavController(){
        return navController;
    }

    protected void childNavUp(int fragmentId){
        getChildNavController().navigateUp();
        getChildNavController().navigate(fragmentId);
    }

    protected void childNavTo(int destination){
        getChildNavController().navigate(destination);
    }

    protected Fragment getNavHostFragment(){
        if (navHostFragment == null){
            navHostFragment = getChildFragmentManager().findFragmentById(getHostFragmentResId());
        }
        return navHostFragment;
    }

    private void onNavChangeListener(NavController navController, NavDestination navDestination, Bundle bundle) {
        try {
            Log.d(TAG, "onNavChangeListener: " + getNavHostFragment().getChildFragmentManager().getBackStackEntryCount());
//                if (getNavHostFragment().getChildFragmentManager().getBackStackEntryCount() == 0) {
////                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                } else {
////                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//                }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    protected abstract int getNavigationGraphResId();

    protected int getHostFragmentResId(){
        return R.id.nav_tab_host_fragment;
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        initGraph(root);
        return root;
    }

    @Override
    protected void initView(View root) {
        super.initView(root);
        tlaTab = root.findViewById(R.id.tlaTab);

        TabLayout.Tab tab;
        for(int index = 0; index < getTabs().size(); index++) {
            tab  = tlaTab.newTab();
            tab.setText(getTabs().get(index));
            tlaTab.addTab(tab);
        }

        tlaTab.getTabAt(getDefaultTabPosition()).select();

        tlaTab.addOnTabSelectedListener(getTabChangeListener());
    }

    protected int getDefaultTabPosition() {
        return 0;
    }

    protected List<String> getTabs(){
        return getTabs("Tab1", "Tab2", "Tab3");
    }

    protected final List<String> getTabs(String... strings) {
        return Arrays.asList(strings);
    }

//    protected TabLayout.Tab getSelectedTab(){
//        return tabLayout.getTabAt(getSelectedTabPosition());
//    }
//
//    protected int getSelectedTabPosition(){
//        return tabLayout.getSelectedTabPosition();
//    }

    protected abstract TabLayout.OnTabSelectedListener getTabChangeListener();
}
