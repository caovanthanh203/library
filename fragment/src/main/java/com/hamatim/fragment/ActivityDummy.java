package com.hamatim.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class ActivityDummy extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initView();
    }

    protected int getLayoutId() {
        return R.layout.layout_fragment_debug;
    }

    protected void initView() {
        findViewById(R.id.btDebugFire).setOnClickListener( v -> onDebug());
        findViewById(R.id.btDebugNoFire).setOnClickListener( v -> onCancel());
    }

    protected abstract void onDebug();
    protected abstract void onCancel();
}
