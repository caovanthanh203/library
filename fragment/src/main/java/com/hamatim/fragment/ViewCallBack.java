package com.hamatim.fragment;

import android.view.View;

public interface ViewCallBack<DTO> {

    void callback(View view, DTO data);

}
