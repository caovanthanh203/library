package com.hamatim.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hamatim.helper.HelperView;

public abstract class FmDummy extends FmBase {

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_debug;
    }

    @Override
    protected void initView(View root) {
        super.initView(root);
        onClick(R.id.btDebugFire, this::onDebug);
        onClick(R.id.btDebugNoFire, this::onCancel);
    }

    protected abstract void onDebug();
    protected abstract void onCancel();
}
