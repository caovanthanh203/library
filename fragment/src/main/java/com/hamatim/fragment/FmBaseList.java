package com.hamatim.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public abstract class FmBaseList<M, A extends AdapterBase> extends FmBase {

    private RecyclerView recyclerView;
    private FloatingActionButton fabAdd;
    private A adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        recyclerView = root.findViewById(getListViewId());
        recyclerView.setLayoutManager(getLayoutManager());
        recyclerView.setAdapter(getAdapter());

        fabAdd = root.findViewById(R.id.fabAdd);

        if(fabAdd != null){
            fabAdd.setOnClickListener(v -> onAddButtonClick());
        }

        return root;
    }

    protected void onAddButtonClick(){
//        throw new RuntimeException("Not implement yet!");
    };

    protected abstract RecyclerView.LayoutManager getLayoutManager();

    protected int getLayoutId(){
        if (canAddNewEntity()){
            return R.layout.layout_fragment_list_fab;
        }
        return R.layout.layout_fragment_list;
    }

    protected boolean canAddNewEntity(){
        return false;
    };

    protected int getListViewId(){
        return R.id.rcvItemList;
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public A getAdapter() {
        if (adapter == null){
            adapter = onCreateAdapter(getContext());
            postAdapterCreated(adapter);
        }
        return adapter;
    }

    protected void postAdapterCreated(A adapter){
        //do some stuff
    };

    @Deprecated
    protected A onCreateAdapter(Context context){
        return onCreateAdapter();
    };

    protected abstract A onCreateAdapter();
}
