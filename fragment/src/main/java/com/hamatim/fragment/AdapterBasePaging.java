package com.hamatim.fragment;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.List;

public abstract class AdapterBasePaging<M, V extends VHBase> extends AdapterBase<M, V>{
    private static final String TAG = "AdapterBasePaging";
    private CallBack<Integer> loadMoreCallback;
    private int page;
    private boolean hasMorePage = true;
    private boolean firstInit = true; //on mvvm list maybe zero when add watcher, before call load

    public AdapterBasePaging(Context mContext) {
        super(mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull V holder, int position) {
        if (isLastPage(position) && isHasMorePage()){
            if (getLoadMoreCallback() != null){
                onLoadPage();
                getLoadMoreCallback().callback(page);
            }
        } else {
            onStopLoad();
        }
    }

    protected void onLoadPage(){};
    protected void onStopLoad(){};

    private boolean isLastPage(int position) {
        return position == page*getPageLimit() - 1;
    }

    public CallBack<Integer> getLoadMoreCallback() {
        return loadMoreCallback;
    }

    @Override
    public void setmList(List<M> mList) {
        if (!(mList == null || mList.size() == 0)){
            getmList().addAll(mList);
            calculatePage();
        } else {
            if (!firstInit) {
                setHasMorePage(false);
                firstInit = false;
            }
        }
    }

    protected void calculatePage(){
        int page = getmList().size() / getPageLimit();
        if (page * getPageLimit() < getmList().size()) {
            page = page + 1;
        }
        setPage(page);
    }

    protected abstract int getPageLimit();

    public void setLoadMoreCallback(CallBack<Integer> loadMoreCallback) {
        this.loadMoreCallback = loadMoreCallback;
    }

    public void resetPage() {
        firstInit = false;
        setPage(0);
        setHasMorePage(true);
        getmList().clear();
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public boolean isHasMorePage() {
        return hasMorePage;
    }

    public void setHasMorePage(boolean hasMorePage) {
        this.hasMorePage = hasMorePage;
    }

}
