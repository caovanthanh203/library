package com.hamatim.fragment;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.List;

public abstract class FmBaseTabViewPager extends FmBase {
    private static final String TAG = "FmBaseTab";
    private TabLayout tlaTab;
    private ViewPager2 viewPager;
    private AdapterViewPager viewPagerAdapter;
    private static List<String> fmNames;
    private static List<? super Fragment> fmList;

    @Override
    protected int getLayoutId() {
        if (useEqualWithTab()) {
            return R.layout.layout_fragment_base_tab_view_pager_fixed_tab;
        } else {
            return R.layout.layout_fragment_base_tab_view_pager;
        }
    }

    protected boolean useEqualWithTab(){
        return false;
    }

//    protected void initGraph(View root){
//        try {
//            getNavController(root).setGraph(getNavigationGraphResId());
////            navController.addOnDestinationChangedListener(this::onNavChangeListener);
//        } catch (Resources.NotFoundException exception){
//            exception.printStackTrace();
//            Log.d(TAG, "initGraph: Does getNavigationGraphResId return valid id?\nEx: R.navigation.your_graph");
//        }
//    }

//    protected NavController getNavController(View root){
//        if (navController == null){
//            navController = Navigation.findNavController(getNavHostFragment().getView());
//        }
//        return navController;
//    }

//    protected NavController getChildNavController(){
//        return navController;
//    }

//    protected void childNavUp(int fragmentId){
//        getChildNavController().navigateUp();
//        getChildNavController().navigate(fragmentId);
//    }
//
//    protected void childNavTo(int destination){
//        getChildNavController().navigate(destination);
//    }
//
//    protected Fragment getNavHostFragment(){
//        if (navHostFragment == null){
//            navHostFragment = getChildFragmentManager().findFragmentById(getHostFragmentResId());
//        }
//        return navHostFragment;
//    }

//    private void onNavChangeListener(NavController navController, NavDestination navDestination, Bundle bundle) {
//        try {
//            Log.d(TAG, "onNavChangeListener: " + getNavHostFragment().getChildFragmentManager().getBackStackEntryCount());
////                if (getNavHostFragment().getChildFragmentManager().getBackStackEntryCount() == 0) {
//////                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
////                } else {
//////                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
////                }
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
//    }

//    protected abstract int getNavigationGraphResId();
//
//    protected int getHostFragmentResId(){
//        return R.id.nav_tab_host_fragment;
//    };

//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View root = super.onCreateView(inflater, container, savedInstanceState);
//        initGraph(root);
//        return root;
//    }

    @Override
    protected void initView(View root) {
        super.initView(root);
        tlaTab = root.findViewById(getTabLayoutResId());
        viewPager = root.findViewById(getViewPagerResId());

//        tlaTab.setTabGravity(TaGr);

//        getWiewPagerAdapter().attachMediator(tlaTab, viewPager);
        viewPager.setAdapter(getWiewPagerAdapter());
        if (getTabChangeListener() != null) {
            tlaTab.addOnTabSelectedListener(getTabChangeListener());
        }
        viewPager.setCurrentItem(getDefaultTabPosition());
        new TabLayoutMediator(tlaTab, viewPager, (tab, position) -> {
            tab.setText(getFragmentListName().get(position));
        }).attach();
    }

    protected int getDefaultTabPosition() {
        return 0;
    }

    protected TabLayout.OnTabSelectedListener getTabChangeListener(){
        return null;
    };

    private FragmentStateAdapter getWiewPagerAdapter() {
        if (thisFragmentWillBeRecreateInSomeCase()){
            viewPagerAdapter = new AdapterViewPager(this);
            viewPagerAdapter.setFragmentlist(getFragmentList());
            viewPagerAdapter.setFragmentListName(getFragmentListName());
        } else if (viewPagerAdapter == null){
            viewPagerAdapter = new AdapterViewPager(this);
            viewPagerAdapter.setFragmentlist(getFragmentList());
            viewPagerAdapter.setFragmentListName(getFragmentListName());
        }
        return viewPagerAdapter;
    }

    //if tab parent fragment is part of navigation graph
    protected abstract boolean thisFragmentWillBeRecreateInSomeCase();

    protected final List<String> getFragmentListName(){
        if (fmNames == null){
            fmNames = createFragmentListName();
        }
        return fmNames;
    }

    protected final List<? super Fragment> getFragmentList(){
        if (fmList == null){
            fmList = createFragmentList();
        }
        return fmList;
    }

    protected int getViewPagerResId(){
        return R.id.wpg2Tab;
    };

    protected int getTabLayoutResId(){
        return R.id.tlaTab;
    };

    protected abstract List<String> createFragmentListName();
    protected abstract List<? super Fragment> createFragmentList();

}
