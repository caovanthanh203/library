package com.hamatim.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public abstract class FmBaseListWrapper<A extends AdapterBaseWrapper<?, ?>> extends FmBase {

    private RecyclerView recyclerView;
    private A adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(getLayoutId(), container, false);
        recyclerView = root.findViewById(getListViewId());
        recyclerView.setLayoutManager(getLayoutManager());
        recyclerView.setAdapter(getAdapter());
        return root;
    }

    protected abstract RecyclerView.LayoutManager getLayoutManager();

    protected int getLayoutId(){
        return R.layout.layout_fragment_list;
    };

    protected int getListViewId(){
        return R.id.rcvItemList;
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public A getAdapter() {
        if (adapter == null){
            adapter = onCreateAdapter(getContext());
            postAdapterCreated(adapter);
        }
        return adapter;
    }

    protected void postAdapterCreated(A adapter){
        //do some stuff
    };

    @Deprecated
    protected A onCreateAdapter(Context context){
        return onCreateAdapter();
    };

    protected A onCreateAdapter(){
        return null;
    };
}
