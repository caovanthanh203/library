package com.hamatim.fragment;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class VHBase<M> extends RecyclerView.ViewHolder {

    private M model;

    public VHBase(@NonNull View itemView) {
        super(itemView);
    }

    public M getModel() {
        return model;
    }

    public void setModel(M model) {
        this.model = model;
    }

    protected <T extends View> T getView(int id){
        return itemView.findViewById(id);
    }

    public void notifyDataSetChanged(){
        onDataSetChanged();
    }

    public abstract void onDataSetChanged();

}
