package com.hamatim.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FmPlaceholder extends FmBase {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        TextView tvPlaceholder = view.findViewById(R.id.tvPlaceholder);
        tvPlaceholder.setText(getClass().getSimpleName());
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_placeholder;
    }
}
