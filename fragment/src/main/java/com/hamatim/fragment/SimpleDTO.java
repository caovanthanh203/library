package com.hamatim.fragment;

public class SimpleDTO {

    private int position;
    private String string;

    public SimpleDTO(int position, String string) {
        this.position = position;
        this.string = string;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
