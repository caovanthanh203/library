package com.hamatim.fragment;

public interface CallBack<DTO> {

    void callback(DTO dto);

}
