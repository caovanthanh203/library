package com.hamatim.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class AdapterBaseWrapper<M, V extends VHBaseWrapper> extends RecyclerView.Adapter<VHBaseWrapper> {

    private AdapterBaseWrapper<?, ? extends VHBaseWrapper> adapterBaseWrapper;
    private List<M> mList;
    private Context mContext;

    public AdapterBaseWrapper(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public VHBaseWrapper onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(mContext).inflate(getLayoutResId(), parent, false);
        VHBaseWrapper view;
        if (viewType == hashCode()){
            view = onCreateViewHolder(root);
            onPostViewCreated(view);
        } else {
            view = getAdapterBaseWrapper().onCreateViewHolder(root);
            getAdapterBaseWrapper().onPostViewCreated(view);
        }
        return view;
    }

    protected void onPostViewCreated(VHBaseWrapper root){};

    protected abstract V onCreateViewHolder(View root);

    protected abstract int getLayoutResId();

    @Override
    public void onBindViewHolder(@NonNull VHBaseWrapper holder, int position) {
        if (position < getmList().size()){
            bindSelfViewHolder((V) holder, position);
        } else {
            if (getAdapterBaseWrapper() != null){
                getAdapterBaseWrapper().onBindViewHolder(holder, position - getmList().size());
            }
        }
    }

    protected abstract void bindSelfViewHolder(V holder, int position);

    @Override
    public int getItemCount() {
        return getmList().size() + getWrapperAdapterCount();
    }

    private int getWrapperAdapterCount() {
        if (getAdapterBaseWrapper() != null){
            return getAdapterBaseWrapper().getItemCount();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return hashCode();
    }

    public List<M> getmList() {
        if (mList == null){
            mList = new ArrayList<>();
        }
        return mList;
    }

    public void setmList(List<M> mList) {
        this.mList = mList;
    }

    public Context getContext(){
        return mContext;
    }

    public void setAdapterBaseWrapper(AdapterBaseWrapper<?, ? extends VHBaseWrapper> adapterBaseWrapper) {
        this.adapterBaseWrapper = adapterBaseWrapper;
    }

    public AdapterBaseWrapper<?, ?> getAdapterBaseWrapper() {
        return this.adapterBaseWrapper;
    }
}
