package com.hamatim.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hamatim.helper.HelperPackage;

public abstract class FmBaseUpgrade extends FmBase {
    private static final String TAG = "FmBaseUpgrade";
    private TextView tvUpgradeName, tvUpgradeValue;
    private ImageView imvUpgradeImage;
    private Button btUpgradeToPro, btUpgradeLater;

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_base_upgrade;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        tvUpgradeName = root.findViewById(R.id.tvFragmentUpgradeName);
        tvUpgradeValue = root.findViewById(R.id.tvUpgradeValue);
        imvUpgradeImage = root.findViewById(R.id.imvUpgradeImage);
        btUpgradeToPro = root.findViewById(R.id.btUpgradeToPro);
        btUpgradeLater = root.findViewById(R.id.btUpgradeLater);

        if (!hasUpgradeIcon()){
            imvUpgradeImage.setVisibility(View.GONE);
        }
        return root;
    }

    //show icon on top of upgrade page (like premium plan icon)
    protected abstract boolean hasUpgradeIcon();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvUpgradeName.setText(getUpgradeName());
        tvUpgradeValue.setText(getUpgradeValues());
        imvUpgradeImage.setImageResource(getUpgradeImageResId());

        btUpgradeToPro.setOnClickListener(view -> onUpgradeToPro());
        btUpgradeLater.setOnClickListener(view -> onUpgradeLater());
    }

    protected void onUpgradeLater(){
        navBack();
    }

    protected void onUpgradeToPro(){
        HelperPackage.openStorePage(getContext(), getUpgradePackageName());
    }

    protected abstract String getUpgradePackageName();

    protected String getUpgradeValues() {
        return getUpgradeValues("Value 1", "Value 2");
    }

    protected final String getUpgradeValues(String... values) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string: values){
            stringBuilder.append(string);
            stringBuilder.append("\n");
        }
        stringBuilder.trimToSize();
        return stringBuilder.toString();
    }

    protected String getUpgradeName() {
        return "Example Pro Version";
    }

    protected int getUpgradeImageResId(){
        return R.mipmap.ic_launcher_mock;
    }



}
