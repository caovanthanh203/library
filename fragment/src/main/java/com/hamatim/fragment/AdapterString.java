package com.hamatim.fragment;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.hamatim.callback.HMTDTOCallBack;

public class AdapterString extends AdapterBase<String, VHString>{

    protected HMTDTOCallBack<SimpleDTO> simpleDTOHMTDTOCallBack;

    public AdapterString(Context mContext) {
        super(mContext);
    }

    @Override
    protected VHString onCreateViewHolder(View root, int type) {
        return new VHString(root);
    }

    @Override
    protected void onPostViewCreated(VHString root, int viewType) {
        super.onPostViewCreated(root, viewType);
        root.itemView.setOnClickListener(v -> {
            if (root.getModel() != null){
                if (getSimpleDTOHMTDTOCallBack() != null){
                    getSimpleDTOHMTDTOCallBack().callback(new SimpleDTO(root.getBindingAdapterPosition(), root.getModel()));
                }
            }
        });
    }

    @Override
    protected int getLayoutResId(int viewType) {
        return VHString.layout;
    }

    @Override
    public void onBindViewHolder(@NonNull VHString holder, int position) {
        holder.setModel(getItem(position));
        holder.onDataSetChanged();
    }

    public HMTDTOCallBack<SimpleDTO> getSimpleDTOHMTDTOCallBack() {
        return simpleDTOHMTDTOCallBack;
    }

    public void setItemClickCallback(HMTDTOCallBack<SimpleDTO> simpleDTOHMTDTOCallBack) {
        this.simpleDTOHMTDTOCallBack = simpleDTOHMTDTOCallBack;
    }
}
