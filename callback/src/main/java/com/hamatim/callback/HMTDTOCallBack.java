package com.hamatim.callback;

public interface HMTDTOCallBack<DTO> {

    void callback(DTO dto);

}
