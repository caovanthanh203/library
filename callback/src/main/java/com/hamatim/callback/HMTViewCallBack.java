package com.hamatim.callback;

import android.view.View;

public interface HMTViewCallBack<DTO> {

    void callback(View view, DTO dto);

}
