package com.hamatim.hmt_base;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

public abstract class ApplicationBaseMultiDex extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
