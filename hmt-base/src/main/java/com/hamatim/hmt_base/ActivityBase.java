package com.hamatim.hmt_base;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kobakei.ratethisapp.RateThisApp;

public abstract class ActivityBase extends AppCompatActivity {

    private static final String TAG = "ActivityBase";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        if (shouldShowRatingDialog()) {
            showRating();
        }
        initHomeMenu();
    }

    private void initHomeMenu() {
        if (getActionBar() != null){
            getActionBar().setDisplayHomeAsUpEnabled(shouldShowHomeMenu());
        }
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShowHomeMenu());
        }
    }

    protected boolean shouldShowHomeMenu() {
        return true;
    }

    protected abstract int getLayoutId();

    protected void notNull(Object object){
        if (object == null){
            throw new RuntimeException("If object is View, check if layout res is correct!");
        }
    }

    public void setTitle(String title){
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
        if (getActionBar() != null) {
            getActionBar().setTitle(title);
        }
    }

    protected void logbox(String message){
        logDivider();
        Log.d(TAG, "logbox: " + message);
        logDivider();
    }

    protected void logDivider() {
        Log.d(TAG, "logbox: ===============================================");
    }

    protected String getApplicationId() {
        return "";
    }

    protected boolean hasRatingMenu() {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (hasRatingMenu() && !getApplicationId().isEmpty()) {
            getMenuInflater().inflate(R.menu.rating, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (itemId == R.id.mnitRating) {
            navToStoreForRating(getApplicationId());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void navToStoreForRating(String applicationId) {
        navToStore(applicationId);
    }

    protected void navToStore(String getApplicationId){
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplicationId)));
        } catch (android.content.ActivityNotFoundException anfe) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationId)));
            } catch (android.content.ActivityNotFoundException ignored) {

            }
        }
    }

    protected boolean shouldShowRatingDialog() {
        return false;
    }

    private void showRating() {
        RateThisApp.init(getRatingConfig());
        RateThisApp.onCreate(this);
        RateThisApp.setCallback(new RateThisApp.Callback() {
            @Override
            public void onYesClicked() {
                onUserAgreeToRating();
            }

            @Override
            public void onNoClicked() {

            }

            @Override
            public void onCancelClicked() {

            }
        });
        RateThisApp.showRateDialogIfNeeded(this);
    }

    protected void onUserAgreeToRating(){

    };

    private RateThisApp.Config getRatingConfig() {
        if (debugRatingDialog()){
            return new RateThisApp.Config(0, 0);
        }
        return new RateThisApp.Config(2, 5);
    }

    protected boolean debugRatingDialog(){
        return false;
    };

}
