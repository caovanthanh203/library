include ':fragment'
include ':fragment-menu'
//include ':fragment-pick-file'
include ':navigation'
//include ':navigation-ads-banner'
//include ':navigation-ads-base'
//include ':navigation-ads-interstitial'
include ':helper'
//include ':worker'
//include ':network'
//include ':network-client'
//include ':viewmodel'
//include ':locale'
//include ':iap'
//include ':google-drive'
include ':callback'
//include ':hmt-splash'
include ':hmt-base'
//include ':hmt-ads'
//include ':hmt-ads-banner'
include ':hmt-db-room'

project(':fragment').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/fragment')
project(':fragment-menu').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/fragment-menu')
project(':navigation').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/navigation')
//project(':navigation-ads-banner').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/navigation-ads-banner')
//project(':navigation-ads-base').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/navigation-ads-base')
//project(':navigation-ads-interstitial').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/navigation-ads-interstitial')
project(':helper').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/helper')
//project(':worker').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/worker')
//project(':network').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/network')
//project(':network-client').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/network-client')
//project(':fragment-pick-file').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/fragment-pick-file')
//project(':viewmodel').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/viewmodel')
//project(':locale').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/locale')
//project(':iap').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/iap')
//project(':google-drive').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/google-drive')
project(':callback').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/callback')
//project(':hmt-splash').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/hmt-splash')
project(':hmt-base').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/hmt-base')
//project(':hmt-ads').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/hmt-ads')
//project(':hmt-ads-banner').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/hmt-ads-banner')
project(':hmt-db-room').projectDir = new File('/mnt/e9b46d64-7bc6-4d9d-a340-b89e1acf7519/module/com-hamatim-library/hmt-db-room')