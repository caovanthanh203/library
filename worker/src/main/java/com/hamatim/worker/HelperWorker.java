package com.hamatim.worker;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.work.Data;
import androidx.work.ListenableWorker;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import java.util.List;
import java.util.UUID;

public class HelperWorker {

    public static OneTimeWorkRequest once(Class<? extends ListenableWorker> tClass, String tag){
        return new OneTimeWorkRequest.Builder(tClass).addTag(tag).build();
    }

    public static OneTimeWorkRequest once(Class<? extends ListenableWorker> tClass, String tag, Data data){
        return new OneTimeWorkRequest.Builder(tClass).addTag(tag).setInputData(data).build();
    }

    public static OneTimeWorkRequest once(Class<? extends ListenableWorker> tClass){
        return new OneTimeWorkRequest.Builder(tClass).build();
    }

    public static UUID runOnce(Context context, Class<? extends ListenableWorker> tClass){
        WorkRequest workRequest = once(tClass);
        getWorkManager(context).enqueue(workRequest);
        return workRequest.getId();
    }

    public static UUID runOnce(Context context, Class<? extends ListenableWorker> tClass, String tag){
        WorkRequest workRequest = once(tClass, tag);
        getWorkManager(context).enqueue(workRequest);
        return workRequest.getId();
    }

    public static UUID runOnce(Context context, Class<? extends ListenableWorker> tClass, String tag, Data data){
        WorkRequest workRequest = once(tClass, tag, data);
        getWorkManager(context).enqueue(workRequest);
        return workRequest.getId();
    }

    public static void watch(Context context, UUID requestId, Fragment fragment, Observer<WorkInfo> observer){
        getWorkInfoLiveData(context, requestId).observe(fragment.getViewLifecycleOwner(), observer);
    }

    public static void watch(Context context, String tag, Fragment fragment, Observer<List<WorkInfo>> observer){
        getWorkInfoLiveDataByTag(context, tag).observe(fragment.getViewLifecycleOwner(), observer);
    }

    private static LiveData<WorkInfo> getWorkInfoLiveData(Context context, UUID requestId) {
        return getWorkManager(context).getWorkInfoByIdLiveData(requestId);
    }

    private static LiveData<List<WorkInfo>> getWorkInfoLiveDataByTag(Context context, String tag) {
        return getWorkManager(context).getWorkInfosByTagLiveData(tag);
    }

    public static WorkManager getWorkManager(Context context) {
        return WorkManager.getInstance(context);
    }

}
