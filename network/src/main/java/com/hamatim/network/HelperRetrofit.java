package com.hamatim.network;

import android.util.Log;

import com.hamatim.helper.HelperSingleton;
import com.hamatim.network_client.ClientBase;

import java.util.HashMap;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HelperRetrofit {

    private static final String TAG = "HelperRetrofit";
    private Retrofit retrofit;
    private static HelperRetrofit instance;
    private String baseUrl;
    private boolean enableLogging, enableTLS;
    private HashMap<String, Object> cached = new HashMap<>();

    public HelperRetrofit() {
    }

    public static HelperRetrofit init(String baseUrl){
        getInstance().setBaseUrl(baseUrl);
        return getInstance();
    }

    private static HelperRetrofit getInstance() {
        return HelperSingleton.get(HelperRetrofit.class);
    }

    public static <T> T getAPI(final Class<T> service) {
        return (T) getInstance().getCachedAPI(service);
    }

    private <T> T getCachedAPI(final Class<T> service){
        if (null == cached.get(service.getCanonicalName())) {
            cached.put(service.getCanonicalName(), getRetrofit().create(service));
        }
        return (T) cached.get(service.getCanonicalName());
    }

    private Retrofit getRetrofit() {
        if (null == retrofit) {
            buildRetrofit();
        }
        return retrofit;
    }

    protected void buildRetrofit(){
        try {
            retrofit = new Retrofit.Builder()
                    .baseUrl(getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(ClientBase.getHttpsClient(getEnableLogging(), getEnableHttps()))
                    .build();
        } catch (Exception exception){
            Log.e(TAG, "getInstance: ======================================");
            throw new RuntimeException("Call HelperRetrofit.init(baseUrl) first!");
        }
    };

    protected boolean getEnableLogging(){
        return enableLogging;
    };

    protected boolean getEnableHttps(){
        return enableTLS;
    }

    protected String getBaseUrl(){
        return baseUrl;
    };

    protected HelperRetrofit setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public HelperRetrofit enableLogging(boolean enableLogging) {
        this.enableLogging = enableLogging;
        return this;
    }

    public HelperRetrofit enableTLS(boolean enableTLS) {
        this.enableTLS = enableTLS;
        return this;
    }

};
