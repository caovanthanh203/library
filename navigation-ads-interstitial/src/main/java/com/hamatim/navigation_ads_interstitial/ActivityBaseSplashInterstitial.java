package com.hamatim.navigation_ads_interstitial;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.hamatim.navigation_ads_interstitial.ActivityBaseInterstitial;

public abstract class ActivityBaseSplashInterstitial extends ActivityBaseInterstitial {

    private static final String TAG = "ActivityBaseSplash";
    private CountDownTimer countDownTimer;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initResource();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_activity_splash;
    }

    protected void initResource() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        intent = new Intent(this, getTargetActivity());

        countDownTimer = new CountDownTimer(getSleepTimeInMillis(), 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                navToTargetActivity();
            }
        }.start();
    }

    protected void navToTargetActivity() {
        showInterstitialAds(() -> {
            startActivity(intent);
            finish();
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        try {
            countDownTimer.cancel();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    protected long getSleepTimeInMillis() {
        return 1000;
    }

    protected abstract Class<? extends AppCompatActivity> getTargetActivity();

}