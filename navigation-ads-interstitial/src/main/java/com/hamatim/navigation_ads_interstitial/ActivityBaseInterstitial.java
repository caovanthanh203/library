package com.hamatim.navigation_ads_interstitial;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.hamatim.helper.VoidCallBack;
import com.hamatim.hmt_ads.ActivityBaseAds;
import com.hamatim.navigation_ads_base.ActivityBaseGraphAds;

public abstract class ActivityBaseInterstitial extends ActivityBaseAds {
    private static final String TAG = "ActivityBaseInterstitia";
    private InterstitialAd mInterstitialAd = null;
    private boolean isAdLoading;

    public static void showInterstitialAds(Activity activity, VoidCallBack voidCallBack){
        Log.d(TAG, "showInterstitialAds: ");
        ActivityBaseInterstitial activityBaseInterstitial = null;
        try {
            activityBaseInterstitial = (ActivityBaseInterstitial) activity;
        } catch (Exception exception){
            exception.printStackTrace();
        }
        if (activityBaseInterstitial != null){
            activityBaseInterstitial.showInterstitialAds(voidCallBack);
        }
    }

    protected void showInterstitialAds(VoidCallBack voidCallBack){
        Log.d(TAG, "showInterstitialAds: ");
        if (mInterstitialAd != null){
            Log.d(TAG, "showInterstitialAds: isLoaded");
            mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback(){
                @Override
                public void onAdShowedFullScreenContent() {
                    super.onAdShowedFullScreenContent();
                    Log.d(TAG, "onAdShowedFullScreenContent:");
                    mInterstitialAd = null;
                }

                @Override
                public void onAdDismissedFullScreenContent() {
                    super.onAdDismissedFullScreenContent();
                    Log.d(TAG, "onAdDismissedFullScreenContent:");
                    onPostResume();
                    voidCallBack.callback();
                    loadNewAds();
                }
            });
            mInterstitialAd.show(this);
        } else {
            Log.d(TAG, "showInterstitialAds: !isLoaded");
            onPostResume();
            voidCallBack.callback();
        }
    }

    protected void loadNewAds(){
        if (!isAdLoading){
            loadAds();
        }
//        if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
//            mInterstitialAd.loadAd(getAdRequest());
//        }
    }

    @Override
    protected void loadAdsWithFakeUnit(AdRequest adRequest) {
        if (mInterstitialAd == null){
            isAdLoading = true;
            InterstitialAd.load(this, getFakeAdsUnitId(), adRequest, getAdsLoadCallBack());
//            mInterstitialAd = new InterstitialAd(this);
//            mInterstitialAd.setAdUnitId(getFakeAdsUnitId());
//            mInterstitialAd.loadAd(adRequest);
        }
    }

    private InterstitialAdLoadCallback getAdsLoadCallBack() {
        return new InterstitialAdLoadCallback(){
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                super.onAdLoaded(interstitialAd);
                mInterstitialAd = interstitialAd;
                isAdLoading = false;
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                mInterstitialAd = null;
                isAdLoading = false;
            }
        };
    }

    @Override
    protected void loadAdsWithRealUnit(AdRequest adRequest) {
        if (mInterstitialAd == null){
            isAdLoading = true;
            InterstitialAd.load(this, getLiveAdsUnitId(), adRequest, getAdsLoadCallBack());
//            mInterstitialAd = new InterstitialAd(this);
//            mInterstitialAd.setAdUnitId(getLiveAdsUnitId());
//            mInterstitialAd.loadAd(adRequest);
        }
    }

}
