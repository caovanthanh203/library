package com.hamatim.navigation;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import com.hamatim.helper.HMTLogger;
import com.hamatim.hmt_base.ActivityBase;

public abstract class ActivityBaseGraph extends ActivityBase {
    private static final String TAG = "ActivityBaseGraph";
    private NavController navController;
    private Fragment navHostFragment;
    private int backStackCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            initGraph();
        } catch (Exception exception){
            HMTLogger.e(TAG, exception);
        }
        try {
            setUpToolbar();
        } catch (Exception exception){
            HMTLogger.e(TAG, exception);
        }
    }

    protected void setUpToolbar(){
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
        } catch (Exception exception){
            HMTLogger.e(TAG, exception);
         }
    };

    protected void initGraph(){
        try {
            getNavController().setGraph(getNavigationGraphResId());
            getNavController().addOnDestinationChangedListener(this::onNavChangeListener);
        } catch (Resources.NotFoundException exception){
            HMTLogger.e(TAG, exception);
            logbox("Does getNavigationGraphResId return valid id?\nEx: R.navigation.your_graph");
        }
    }

    protected NavController getNavController(){
        if (navController == null){
            navController = Navigation.findNavController(this, getHostFragmentResId());
        }
        return navController;
    }

    protected Fragment getNavHostFragment(){
        if (navHostFragment == null){
            navHostFragment = getSupportFragmentManager().findFragmentById(getHostFragmentResId());
        }
        return navHostFragment;
    }

    private void onNavChangeListener(NavController navController, NavDestination navDestination, Bundle bundle) {
        if (autoShowHomeButton()) {
//            try {
//                Log.d(TAG, "onNavChangeListener: " +getNavHostFragment().getChildFragmentManager().getBackStackEntryAt(1).getClass().getCanonicalName());
//            } catch (Exception exception){
//                Log.d(TAG, "onNavChangeListener: " );
//                exception.printStackTrace();
//            }
            try {
//                Log.d(TAG, "onNavChangeListener: " + getNavHostFragment().getChildFragmentManager().getFragments().size());
                Log.d(TAG, "onNavChangeListenerChild: " + getNavHostFragment().getChildFragmentManager().getBackStackEntryCount());
                if (navDestination.getId() != getHomeDestination()) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            } catch (Exception exception) {
                HMTLogger.e(TAG, exception);
            }
        }
    }

    protected abstract int getHomeDestination();

    protected boolean autoShowHomeButton() {
        return true;
    }

    public int getHostFragmentResId() {
        return R.id.nav_host_fragment;
    }

    protected abstract int getNavigationGraphResId();

}
