package com.hamatim.hmt_ads_interstitial;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.hamatim.callback.HMTVoidCallBack;
import com.hamatim.helper.VoidCallBack;
import com.hamatim.hmt_ads.HelperAds;

public class HMTAdsInterstitial {

    private static final String TAG = "HMTAdsInterstitial";
    private InterstitialAd mInterstitialAd = null;
    private boolean isAdLoading;
    private static HMTAdsInterstitial instance;
    private Context context;
    private static boolean forceTestAdsWithRealId;
    private static boolean forceDisableAds = false;

    private static final String TEST_ADMOB_UNIT_ID = "ca-app-pub-3940256099942544/1033173712";
    private static String LIVE_ADMOB_UNIT_ID = "ca-app-pub-3940256099942544/1033173712";
    private static long lastShowTime = 0;
    private static int timePaddingInMillis = 60000;

    private HMTAdsInterstitial(Context context) {
        this.context = context;
    }

    public static void init(Application application) {
        createInstance(application);
//        if (shouldServeAds()) {
//            initAdSDK();
//        } else {
//            hideAds();
//        }
    }

    private static void createInstance(Context context) {
        if (null == instance) {
            instance = new HMTAdsInterstitial(context);
        }
    }

    protected void loadAdsWithRealUnit(AdRequest adRequest) {
        if (mInterstitialAd == null){
            isAdLoading = true;
            InterstitialAd.load(getContext(), getLiveAdsUnitId(), adRequest, getAdsLoadCallBack());
        }
    }

    private static HMTAdsInterstitial getInstance() {
        if (null == instance) {
            throw new RuntimeException("You have to call HMTAdsInterstitial.init(applicationContext) first!!!");
        }
        return instance;
    }

    public static HMTAdsInterstitial get() {
        return getInstance();
    }

    private Context getContext() {
        return context;
    }

    protected void loadAdsWithFakeUnit(AdRequest adRequest) {
        if (mInterstitialAd == null){
            isAdLoading = true;
            InterstitialAd.load(getContext(), getFakeAdsUnitId(), adRequest, getAdsLoadCallBack());
        }
    }

    protected InterstitialAdLoadCallback getAdsLoadCallBack() {
        return new InterstitialAdLoadCallback(){
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                super.onAdLoaded(interstitialAd);
                mInterstitialAd = interstitialAd;
                isAdLoading = false;
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                mInterstitialAd = null;
                isAdLoading = false;
            }
        };
    }

    public static HMTAdsInterstitial setLiveAdUnitId(String value) {
        LIVE_ADMOB_UNIT_ID = value;
        return get();
    }

    public static HMTAdsInterstitial forceTestWithRealId(boolean value){
        forceTestAdsWithRealId = value;
        return get();
    }

    public static HMTAdsInterstitial setTimePadding(long padding){
        if (padding < 30000){
            throw new RuntimeException("Padding time to show ads should not less than 30000 millis");
        }
        return get();
    }

    protected String getLiveAdsUnitId() {
        return LIVE_ADMOB_UNIT_ID;
    }

    protected String getFakeAdsUnitId() {
        return TEST_ADMOB_UNIT_ID;
    }

    public void showAds(Activity activity, HMTVoidCallBack callBack){
        if (!enoughTimePassed()){
            Log.d(TAG, "showAds: too fast!");
            if (callBack != null){
                callBack.callback();
            }
            return;
        }
        Log.d(TAG, "showInterstitialAds: ");
        if (mInterstitialAd != null){
            Log.d(TAG, "showInterstitialAds: isLoaded");
            mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback(){
                @Override
                public void onAdShowedFullScreenContent() {
                    super.onAdShowedFullScreenContent();
                    Log.d(TAG, "onAdShowedFullScreenContent:");
                    mInterstitialAd = null;
                    lastShowTime = System.currentTimeMillis();
                }

                @Override
                public void onAdDismissedFullScreenContent() {
                    super.onAdDismissedFullScreenContent();
                    Log.d(TAG, "onAdDismissedFullScreenContent:");
                    if (callBack != null) {
                        callBack.callback();
                    }
                    loadNewAds();
                }
            });
            mInterstitialAd.show(activity);
        } else {
            Log.d(TAG, "showInterstitialAds: !isLoaded");
            if (callBack != null) {
                callBack.callback();
            }
        }
    }

    private boolean enoughTimePassed() {
        return System.currentTimeMillis() - lastShowTime > timePaddingInMillis;
    }

    public void loadNewAds(){
        if (!isAdLoading){
            Log.d(TAG, "loadNewAds");
            loadAdmobAds(getAdRequest());
        }
//        if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
//            mInterstitialAd.loadAd(getAdRequest());
//        }
    }

    public void loadNewAdsAndShow(Activity context, VoidCallBack callBack){
        if (!isAdLoading && enoughTimePassed()){
            Log.d(TAG, "loadNewAds");
            if (testAdsWithRealId()){
                loadAdsAndShow(context, getLiveAdsUnitId(), callBack);
            } else {
                if (testAdsWithFakeId(getAdRequest())) {
                    Log.d("Ads", "This is test device");
                    loadAdsAndShow(context, getFakeAdsUnitId(), callBack);
                } else {
                    loadAdsAndShow(context, getLiveAdsUnitId(), callBack);
                }
            }
        }
//        if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
//            mInterstitialAd.loadAd(getAdRequest());
//        }
    }

    private void loadAdsAndShow(Activity activity, String adId, VoidCallBack callBack) {
        InterstitialAd.load(getContext(), adId, getAdRequest(), new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                super.onAdLoaded(interstitialAd);
                interstitialAd.setFullScreenContentCallback(new FullScreenContentCallback(){
                    @Override
                    public void onAdShowedFullScreenContent() {
                        super.onAdShowedFullScreenContent();
                        Log.d(TAG, "onAdShowedFullScreenContent:");
                        lastShowTime = System.currentTimeMillis();
                    }

                    @Override
                    public void onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent();
                        Log.d(TAG, "onAdDismissedFullScreenContent:");
                        callBack.callback();
                    }
                });
                activity.finish();
                interstitialAd.show(activity);
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
    }

    private void loadAdmobAds(AdRequest adRequest) {
        if (testAdsWithRealId()){
            loadAdsWithRealUnit(adRequest);
        } else {
            if (testAdsWithFakeId(adRequest)) {
                Log.d("Ads", "This is test device");
                loadAdsWithFakeUnit(adRequest);
            } else {
                loadAdsWithRealUnit(adRequest);
            }
        }
    }

    private void loadAdmobAds(AdRequest adRequest, VoidCallBack callBack) {

    }

    protected AdRequest getAdRequest() {
        return new AdRequest.Builder().build();
    }

    protected boolean testAdsWithRealId(){
        if (forceTestAdsWithRealId()) {
            Log.e(TAG, "testAdsWithRealId: ==============================================");
            Log.e(TAG, "testAdsWithRealId: Remember to turn on test ads on networks!!!!!!");
            Log.e(TAG, "testAdsWithRealId: ==============================================");
        }
        return forceTestAdsWithRealId() || HelperAds.hasTestWithRealAdsKey(context.getPackageManager());
    }

    protected boolean testAdsWithFakeId(AdRequest adRequest) {
        return HelperAds.hasTestKey(context.getPackageManager()) || adRequest.isTestDevice(context);
    }

    protected boolean forceTestAdsWithRealId(){
        return forceTestAdsWithRealId;
    };

    protected boolean forceNotLoadAds(){
        return false;
    };

//    private boolean shouldServeAds() {
//        if (forceNotLoadAds()){ //is force to hide
//            Log.e(TAG, "loadPersonalizedAds: ==============================================");
//            Log.e(TAG, "loadPersonalizedAds: forceNotLoadAds!!!!!!");
//            Log.e(TAG, "loadPersonalizedAds: ==============================================");
//            return false;
//        }
//
//        if (shouldCheckFraud()) {
//            if (!HMTAdsLimit.isSafe(this)) {
//                return false;
//            }
//        }
//
//        if (!HelperAds.isAdsReadyToServe(this)){
//            String dateString = String.format(
//                    "%s %s %s",
//                    getString(com.hamatim.navigation_ads_base.R.string.ads_ready_date),
//                    getString(com.hamatim.navigation_ads_base.R.string.ads_ready_time),
//                    getString(com.hamatim.navigation_ads_base.R.string.ads_ready_zone)
//            );
//            Log.e(TAG, "loadPersonalizedAds: ==============================================");
//            Log.e(TAG, "loadPersonalizedAds: Not ready to serve ads!!!!!!");
//            Log.e(TAG, "loadPersonalizedAds: Wait to " + dateString);
//            Log.e(TAG, "loadPersonalizedAds: ==============================================");
//            return false;
//        }
//
//
//        if (!isFreeWithAdsUser()){ //is pro
//            Log.e(TAG, "loadPersonalizedAds: ==============================================");
//            Log.e(TAG, "loadPersonalizedAds: User is premium, not load ads!!!!!!");
//            Log.e(TAG, "loadPersonalizedAds: ==============================================");
//            return false;
//        }
//        return true;
//    }

}
